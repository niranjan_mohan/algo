package maze;

import java.util.ArrayList;
import java.util.List;

public class Boggle {

    public List<String> getWords(char [][] game,List<String> dict){
        if(game ==null){
            return null;
        }
        TrieNode node = getTrieNodeFromList(dict);
        int len = game.length;
        List<String> result = new ArrayList<String>();
        for(int i=0;i<len;i++){
            for(int j=0;j<len;j++){
                searchForWord(game,node,result,i,j);
            }
        }

        return result;
    }


    public void searchForWord(char [][] game, TrieNode root, List<String> result, int i, int j){
        if(i < 0 || j<0 || i >= game.length || j >= game.length ){
            return;
        }
        if(game[i][j] == '#' || root.getNode('A'- game[i][j]) == null){
            return;
        }
        char c = game[i][j];
        game[i][j] ='#';
        TrieNode newNode = root.getNode('A'-c);
        if(newNode.value !=null){
            result.add(newNode.value);
        }

        searchForWord(game,newNode,result,i-1,j);
        searchForWord(game,newNode,result,i+1,j);
        searchForWord(game,newNode,result,i-1,j-1);
        searchForWord(game,newNode,result,i+1,j+1);
        searchForWord(game,newNode,result,i,j-1);
        searchForWord(game,newNode,result,i,j+1);

        game[i][j]=c;

    }


    public TrieNode getTrieNodeFromList(List<String> dict){
        if(dict ==null) return null;
        TrieNode root = new TrieNode();
        for(String s:dict){
            root.addString(s,0);
        }
        return root;
    }

    class TrieNode{
        String value;
        TrieNode[] child;
        TrieNode(){
            value =null;
            child =new TrieNode[26];
        }


        public TrieNode getNode(int index){
            if(child[index] != null){
                return child[index];
            }else{
                return null;
            }
        }

        public void addString(String value,int index){
            if(index >= value.length()) return;
            char c = value.charAt(index);
            int cIndex = 'A' - c;
            if(child[cIndex] == null){
                child[cIndex] = new TrieNode();
            }
            if(cIndex == value.length()-1){
                this.value =value;
                return;
            }else{
                child[cIndex].addString(value,index+1);
            }
        }
    }
}
