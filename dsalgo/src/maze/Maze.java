package maze;

import java.util.Arrays;

/**
 * Created by nirmohan on 8/18/17.
 */
public class Maze {


    public static void main(String []args){
        //test code goes here
        Maze maze = new Maze();
        int rows =5;
        int cols =5;
        int [][]dp = new int[rows][cols];

        for (int[] d:dp)
            Arrays.fill(d,-1);

        System.out.println("iterative paths::"+maze.getPathsIterative(rows,cols,dp));



        int [][]dp1 = new int[rows+1][cols+1];
        for (int[] d:dp)
            Arrays.fill(d,-1);


        System.out.println("Recursive paths ::"+maze.getPaths(dp,rows-1,cols-1));

    }

    public int getPaths(int maze[][], int rows,int cols){

        //invalid cell
        if(rows <0 || cols <0){
            return 0;
        }
        //base case
        if(rows ==0 || cols ==0){
            return 1;
        }

        //memoization
        if(maze[rows][cols] !=-1)
            return maze[rows][cols];

        maze[rows][cols] = getPaths(maze,rows-1,cols) + getPaths(maze,rows,cols-1);
        return maze[rows][cols];
    }







    //
    public int getPathsIterative(int rows,int cols,int maze[][]){
        for(int i=0;i<rows;i++){
            maze[i][0] = 1;
        }
        for(int j=0;j<cols;j++){
            maze[0][j] =1;
        }
        for(int i=1;i<rows;i++)
            for(int j=1;j<cols;j++){
                maze[i][j] = maze[i-1][j]+maze[i][j-1];
            }
        return maze[rows-1][cols-1];
    }


    // paths with maze blocked for certain cells
    //http://www.techiedelight.com/find-total-number-unique-paths-maze-source-destination/
    //https://www.cs.bu.edu/teaching/alg/maze/


    // 1,1

    //00 10 11
    //00 01 11

    //2,2
    // 00 10 11 12 22
    // 00 01 11 21 22


    // 4,4   4,3 -> 4,2 -> 4,1 -> 4,0


}
