package graphs.algos;

import java.util.*;



public class MyWeightedGraph {

    Map<Node,LinkedList<Edge>> graph = new HashMap<Node,LinkedList<Edge>>();
    List<Node> nodeList = new ArrayList<Node>();
    //Maintian edge list for bellman ford
    List<Edge> edges = new ArrayList<Edge>();



    public void createGraph(){
       for(int i=0;i<6;i++){
           createNode(i);
       }
       createEdges();
    }


    public  Map<Node,Integer>  bellmenFord() throws Exception{
        Map<Node,Integer> result = new HashMap<Node,Integer>();
        Queue <Node> queue = new LinkedList<Node>();
        for(int i=0;i<nodeList.size();i++){
            Node node = nodeList.get(i);
            for(Edge edge:graph.get(node)){
                relaxBF(result,edge);
            }
        }

        //TODO :detect negative cycle here
        for(Edge edge:edges){
            if(relaxBF(result,edge)){
                throw new Exception("Cycle present");
            }
        }
        return result;
    }


    public boolean relaxBF(Map<Node,Integer> result,Edge edge) {
        boolean isUpdate = false;
        if (result.get(edge.target) > edge.source.weight + edge.weight) {
            //update value
            result.put(edge.target, edge.source.weight + edge.weight);
            edge.target.weight = edge.source.weight+edge.weight;
            isUpdate = true;
        }
        return isUpdate;
    }

    /**
     *  Runs in (O(E) + Vlog(V))
     * @return
     */

    public Map<Node,Integer> djkstra(){
        Map<Node,Integer> result = new HashMap<Node,Integer>();
        BinaryMinHeap heap = new BinaryMinHeap();
        for(Node node:nodeList){
            heap.insert(node);
        }
        heap.buildMinHeap();
        while(heap.peak()!=null){
            Node n = heap.poll();
            List<Edge> edges = graph.get(n);
            for(Edge edge:edges){
                relax(result,edge,heap);
            }
        }
        return result;
    }
    public void relax(Map<Node,Integer> result,Edge edge,BinaryMinHeap heap){
        if(result.get(edge.target) > edge.source.weight+ edge.weight){
            //update value
            heap.updateValue(edge.target,edge.source.weight+edge.weight);
            result.put(edge.target,edge.source.weight+edge.weight);
            edge.target.weight = edge.source.weight+edge.weight;
        }
    }


    private void createEdges(){
        addEdge(nodeList.get(0),nodeList.get(1),3);
        addEdge(nodeList.get(0),nodeList.get(2),5);
    }




    //helper methods
    private void createNode(int id){
        Node n = new Node(id);
        n.weight = Integer.MAX_VALUE;
    }
    private void addEdge(Node source,Node target,int weight){
        Edge edge = new Edge(source,target,weight);
        LinkedList<Edge> ll;
        if(graph.containsKey(source)){
            ll =graph.get(source);
        }else{
             ll= new LinkedList<Edge>();
        }
        ll.add(edge);
    }


/*
1.  create a graph F (a set of trees), where each vertex in the graph is a separate tree
2.  create a set S containing all the edges in the graph
3.  while S is nonempty and F is not yet spanning
       3.1 remove an edge with minimum weight from S
       3.2 if the removed edge connects two different trees then add it to the forest F, combining two trees into a single tree


 */

    public List<Edge> kruskals(int n,List<Edge> edges,List<Node> nodes){
       Collections.sort(edges, Comparator.comparing(Edge::getWeight));

        DisjointSet dj = new DisjointSet();
        for(Node node:nodes){
            dj.makeSet(node.id);
        }

        List<Edge> result = new ArrayList<Edge>();
        for(Edge edge:edges){
            Node sourceParent = dj.findSet(edge.source);
            Node targetParent = dj.findSet(edge.target);
            if(sourceParent == targetParent){
                continue;
            }else{
                dj.union(edge.source,edge.target);
                result.add(edge);
            }
        }

    return result;

    }













}
