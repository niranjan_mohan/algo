package graphs.algos;

import java.util.*;

// TODO : Not tested runtime

public class MyGraphAlgo {


    List<Node> nodeList;
    Map<Integer,Node> nodeMap;
    Node rootNode;


    public void addNode(int value){
        Node node = new Node(value);
        if(rootNode ==null){
            rootNode = node;
            nodeList.add(rootNode);
            nodeMap.put(value,rootNode);
        }else{
            nodeList.add(node);
            nodeMap.put(value,node);
        }
    }

    public void connectNode(int index1, int index2){
        Node n1 = nodeList.get(index1);
        n1.addNode(index2);
    }

    public Node getUnvisitedChild(Node node){
        Node n = nodeMap.get(node.value);
        List<Integer> children = n.getChildren();
        for(Integer i:children){
            Node child = nodeList.get(i);
            if(child.state == State.INITIAL)
                return child;
        }
        return null;
    }


    public void dfs(){
        Stack <Node> s = new Stack<Node>();
        s.push(rootNode);
        while(!s.isEmpty()){
            Node n = s.peek();
            Node child = getUnvisitedChild(n);
            while(child !=null && !(child.state == State.INITIAL)){
                s.push(child);
            }
            n.state = State.COMPLETED;
            s.pop();
            System.out.print(n);
        }
    }

    public void bfs(){
        Queue<Node> q = new LinkedList<Node>();
        q.offer(rootNode);
        while(!q.isEmpty()){
            Node n = q.peek();
            Node child = getUnvisitedChild(n);
            while(child !=null && !(child.state == State.INITIAL)){
                q.offer(child);
            }
            n.state = State.COMPLETED;
            q.poll();
            System.out.print(n);
        }
    }



    public void topologicalSort(){
        Stack<Node> stack = new Stack<Node>();
        for(Node n:nodeList){
            if(!doDfs(n,stack)){
                stack  = null;
            }
        }
    }

    public boolean doDfs(Node node,Stack<Node> stack){
        if(node.state == State.COMPLETED){
            return true;
        }
        if(node.state == State.PARTIAL){
            return false;
        }
        node.state = State.PARTIAL;
        for(Integer i :node.getChildren()){
            Node child = nodeList.get(i);
            if(!doDfs(child,stack)){
                return false;
            }
        }
        node.state = State.COMPLETED;
        stack.push(node);
        return true;
    }


    public class Node {
        int value;
        State state;
        LinkedList<Integer> children;

        Node(int value) {
            this.value = value;
            this.state = State.INITIAL;
            children = new LinkedList<Integer>();
        }

        public void addNode(int index) {
            children.add(index);
        }

        public List<Integer> getChildren() {
            return this.children;
        }
    }

    enum State{
        PARTIAL,
        COMPLETED,
        INITIAL;
    }

    public int minDist(int[] dist,boolean [] isVisited){
        int minValue = Integer.MAX_VALUE;
        int minIndex = -1;
        for(int i=0;i<dist.length;i++){
            if(!isVisited[i] && dist[i] < minValue){
                minValue = dist[i];
                minIndex = i;
            }
        }
        return minIndex;
    }

}
