package graphs.algos;





class Edge {
    Node source;
    Node target;
    Integer weight;
    Edge (Node source,Node target,int weight){
        this.source = source;
        this.target = target;
        this.weight = weight;
    }
    public int getWeight(){
        return this.weight;
    }

}