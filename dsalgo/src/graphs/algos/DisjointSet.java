package graphs.algos;

/**
 *
 * Operations
 *
 * makeSet
 * findSet
 * Union
 *
 *  *
 * For m operations and total n elements time complexity is O(m*f(n)) where f(n) is
 * very slowly growing function. For most cases f(n) <= 4 so effectively
 * total time will be O(m). Proof in Coreman book.

 */
public class DisjointSet {

    public Node makeSet(int index){
        Node n = new Node(index);
        n.setParent(n);
        n.rank = 1;
        return n;
    }


    public Node findSet(Node node){
        Node cur = node;
        while(node.parent.id != node.id){
            node = node.parent;
        }

        //compress
        if(cur.parent.id != node.id){
            cur.parent = node;
        }
        //end compress
        return node;
    }


    public boolean union(Node node1 ,Node node2){
        //check if it is the root Node

        Node parent1 = findSet(node1);
        Node parent2 = findSet(node2);

        if(parent1.parent == parent2.parent){
            return false;
        }
        //see which one to merge
        if(parent1.rank <= parent2.rank){
            parent2.parent.rank = (parent1.rank == parent2.rank) ? parent1.rank + 1 : parent1.rank;
            parent1.parent = parent2;
        }else{
            parent1.parent.rank+=1;
            parent2.parent = parent1;
        }
        return true;
    }






}
