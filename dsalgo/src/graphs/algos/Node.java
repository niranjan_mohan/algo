package graphs.algos;

/**
 *
 *
 *
 */
public class Node implements Comparable<Node>{
    Integer weight;
    Integer id;
    State state;


    //this is required for Disjoint set datastructure
    Node parent;
    int rank;

    Node(int id){
        this.id = id;
        state = State.INITIAL;
        weight = Integer.MAX_VALUE;
    }

    @Override
    public int compareTo(Node node){
        return this.weight.compareTo(node.weight);
    }

    public Node getParent(){
        return  this.parent;
    }
    public void setParent(Node parentNode){
        this.parent = parentNode;
    }



    enum State{
        INITIAL,
        PARTIAL,
        COMPLETE
    }


}
