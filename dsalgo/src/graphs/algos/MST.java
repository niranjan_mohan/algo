package graphs.algos;


import java.util.*;

/**

 Kurskals :

create disjoin sets of all vertices
sort all edges in increasing order
iterate via edges and see if vetrices are in the same set or not


 **/

public class MST {

    List<Node> nodeList = new ArrayList<Node>();
    Map<Node,Edge> graph = new HashMap<Node,Edge>();
    List<Edge> edges = new ArrayList<Edge>();


    public List<Edge> kruskal(){
        //sort edges
        PriorityQueue<Edge> pq = new PriorityQueue<Edge>(new Comparator<Edge>() {
            @Override
            public int compare(Edge o1, Edge o2) {
                return Integer.valueOf(o1.weight).compareTo(o2.weight);
            }
        });


        List<Edge> result = new ArrayList<Edge>();
        DisjointSet set = new DisjointSet();



        while(!pq.isEmpty()){
            Edge edge = pq.poll();
            Node source = edge.source;
            Node target = edge.target;
            if(set.union(source,target)){
                result.add(edge);
            }
        }
        return result;
    }

    public void makeGraph(){

    }





}
