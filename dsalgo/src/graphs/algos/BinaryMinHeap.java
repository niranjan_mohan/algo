package graphs.algos;

import java.util.List;
import java.util.Map;



/**
 *
 * Data structure to support following operations
 * extracMin - O(logn)
 * addToHeap - O(logn)
 * containsKey - O(1)
 * decreaseKey - O(logn)
 * getKeyWeight - O(1)
 *
 * It is a combination of binary heap and hash map
 *
 */


public class BinaryMinHeap {

    // Array maintained as heap
    List<Node> nodes;
    //value of map holds the index of the node in the nodes list
    Map<Node,Integer> nodeIndexMap;
    Node rootNode;


    public Node peak(){
        if(nodes.size() >0){
            return nodes.get(0);
        }
        return null;
    }


    public Node poll(){
        int lastindex = nodes.size()-1;
        swap(0,lastindex);
        nodeIndexMap.remove(nodes.get(lastindex));
        Node node =nodes.remove(lastindex);
        return node;
    }

    public void insert(Node node){
        //call heapify and then insert;
        nodes.add(node);
        nodeIndexMap.put(node,nodes.size()-1);
        heapify(0,nodes.size());

    }

    public void buildMinHeap(){
        int count = (nodes.size() -2)/2;
        while(count >=0){
            heapify(count, nodes.size()-1);
            count --;
        }
    }
    private void heapify(int start,int end){
        int root = start;int child;
        while((root*2+1) < end){
            child = root*2+1;
            if((child+1) < end && nodes.get(child).compareTo(nodes.get(child+1)) > 1){
                child = child+1;
            }
            if(nodes.get(root).compareTo(nodes.get(child))> 1){
                swap(root,child);
                root = child;
            }
        }
    }


    public void updateValue(Node node){
        int index = nodeIndexMap.get(node);
        moveUpValue(index);
    }

    private void moveUpValue(int index){
        if(index <= 0 )
            return;
        if(index ==1 && nodes.get(0).compareTo(nodes.get(1)) >1 ){
            swap(0,1);
        }else{
            int parentIndex;
            int childIndex = index;
            while((index-1)/2 >0){
                parentIndex = (index-1)/2;
                if(nodes.get(parentIndex).compareTo(nodes.get(childIndex)) >1){
                    swap(parentIndex,childIndex);
                }else{
                    break;
                }
            }
        }
    }

    private void swap (int index1,int index2){
        Node temp = nodes.get(index1);
        nodeIndexMap.put(temp,index2);
        nodeIndexMap.put(nodes.get(index2),index1);
        nodes.set(index1,nodes.get(index2));
        nodes.set(index2,temp);
    }

    public void updateValue(Node node, int weight){
        int rootIndex = nodeIndexMap.get(node);

        heapify(rootIndex,nodes.size()-1);
    }


    private void updateHeapPosition(int nodeIndex){
        int parentIndex = calculateParentIndex(nodeIndex);
        while(parentIndex >=0) {
            if (nodes.get(parentIndex).compareTo(nodes.get(nodeIndex)) > 1) {
                swap(nodeIndex, parentIndex);
            }
            nodeIndex = parentIndex;
        }

    }


    private int calculateParentIndex(int index){
        if(index !=0)
            return index/2;
        return 0;
    }

    public void deleteNode(Node node){
        if(nodeIndexMap.containsKey(node)){
            int index = nodeIndexMap.get(node);

        }
    }


}
