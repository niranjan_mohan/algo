package graphs.leetcode;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Solution {


    /*
    todo : https://leetcode.com/problems/cheapest-flights-within-k-stops/solution/


     */


    /*

         Example 1:
        Input:
        n = 3, edges = [[0,1,100],[1,2,100],[0,2,500]]
        src = 0, dst = 2, k = 1
        Output: 200
        Explanation:

        https://leetcode.com/problems/cheapest-flights-within-k-stops/description/

     */


        public int findCheapestPriceF(int n, int[][] flights, int src, int dst, int K) {
            int[] dis = new int[n];
            int[] pre = new int[n];
            Arrays.fill(dis, Integer.MAX_VALUE / 2);
            Arrays.fill(pre, Integer.MAX_VALUE / 2);
            dis[src] = pre[src] = 0;

            for (int i = 0; i <= K; ++i) {
                for (int[] edge: flights)
                    dis[edge[1]] = Math.min(dis[edge[1]], pre[edge[0]] + edge[2]);

                pre = dis;
            }

            return dis[dst]  < Integer.MAX_VALUE / 2 ? dis[dst] : -1;
        }


    public int findCheapestPrice(int n, int[][] flights, int src, int dst, int K) {
        int[][] graph = new int[n][n];
        for (int[] flight: flights)
            graph[flight[0]][flight[1]] = flight[2];

        Map<int[], Integer> best = new HashMap();

        PriorityQueue<int[]> pq = new PriorityQueue<int[]>((a, b) -> a[0] - b[0]);
        pq.offer(new int[]{0, 0, src});

        while (!pq.isEmpty()) {
            int[] info = pq.poll();
            int cost = info[0], k = info[1], place = info[2];
            if (k > K+1 || cost > best.getOrDefault(new int[]{k, place}, Integer.MAX_VALUE))
                continue;
            if (place == dst)
                return cost;

            for (int nei = 0; nei < n; ++nei) if (graph[place][nei] > 0) {
                int newcost = cost + graph[place][nei];
                if (newcost < best.getOrDefault(new int[]{k+1, nei}, Integer.MAX_VALUE)) {
                    pq.offer(new int[]{newcost, k+1, nei});
                    best.put(new int[]{k+1, nei}, newcost);
                }
            }
        }

        return -1;
    }

}
