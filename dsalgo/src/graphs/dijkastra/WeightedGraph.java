package graphs.dijkastra;

import com.sun.javafx.geom.Edge;

import java.util.*;
import java.util.LinkedList;


public class WeightedGraph {


    Map<Node,List<Edge>> nodeList;
    Map<Node,Node> parentNode;
    Queue<Node> unvisited;
    Queue<Node> visited;
    int size;
    Node curNode;
    Map <Node,Integer> dist;// this is the distance from source node to another
    List<Node> path;



    public WeightedGraph(){
        nodeList = new HashMap<Node,List<Edge>>();
        unvisited = new PriorityQueue<Node>(new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                return dist.get(o1).compareTo(dist.get(o2));
            }
        });
        visited = new LinkedList<Node>();
    }
    public void addNode(String name){
        Node n = new Node(name);
        Map<Node,List<Edge>> nodeMap = new HashMap<Node,List<Edge>>();
        nodeMap.put(n,new ArrayList<Edge>());
    }
    public void connectNode(Node source,Node destination,int weight){
        if(nodeList.containsKey(source)){
            //add edge
            Edge edge = new Edge(destination,weight);
            nodeList.get(source).add(edge);
        }
    }


    public void dijkstraSp(Node source,Node destination){

        //intialize the nodes add all nodes to unvisited
//        for(Map.Entry<Node,List<Edge>>map: nodeList.entrySet()){
//            unvisited.add(map.getKey());
//        }


        //intialize source node to 0 dist,
        dist = new HashMap<Node, Integer>() ;
        for(Node n:nodeList.keySet() ){
            dist.put(n,Integer.MAX_VALUE);
        }
        dist.put(source,0);

        unvisited.add(source);
        //get minimum edge update dist and continue till all are visited
            while(!unvisited.isEmpty()) {
            Node n =getMinUnvisitedEdge();
            if(n.equals(destination)){
                System.out.println("distance :"+dist.get(destination));
                break;
            }
        }

    }
    public Node getMinUnvisitedEdge(){

        Node parent = unvisited.poll();
        visited.offer(parent);
        //add neighbours to unvisited list
        List<Edge> edges =nodeList.get(parent);
        for (Edge edge:edges){
            if(dist.get(edge.dNode) < dist.get(parent)+edge.weight) {
                dist.put(edge.dNode, dist.get(parent) + edge.weight);
                parentNode.put(edge.dNode,parent);
            }
            unvisited.offer(edge.dNode);
        }
        return unvisited.peek();
    }



    class Node{
        boolean isVisited;
        String name;
        Node(String name){
            this.name = name;
        }
        @Override
        public int hashCode(){
            return name.hashCode();
        }
    }
    class Edge{
        public Node getdNode() {
            return dNode;
        }

        public void setdNode(Node dNode) {
            this.dNode = dNode;
        }

        Node dNode;

        public int getWeight() {
            return weight;
        }

        public void setWeight(int weight) {
            this.weight = weight;
        }

        int weight;
        Edge(Node dNode,int weight){
            this.dNode = dNode;
            this.weight = weight;
        }

    }
}
