package graphs.TopologicalSort;

import java.util.*;

/**
 *
 *
 * input  // Project ->String [] projects Dependencies ->String [][] dependencies
 * return the ordering of the projects String [] projects -> order in which they should be executed
 *
 *
 *
 *  input ::  projects: a, b, c, d, e, f
 *  input :: dependencies: (a, d), (f, b), (b, d), (f, a), (d, c)
 *  Output:: f, e, a, b, d, c (one of the possible ordering)
 *
 * */



public class ProjectOrder {
    Graph graph;



    public static void main(String []args){
        ProjectOrder projectOrder = new ProjectOrder();

        String [] projects = {"a", "b", "c", "d", "e", "f"};
        String [][] dependencies = {{"a", "d"}, {"f", "b"}, {"b", "d"}, {"f", "a"}, {"d", "c"}};


        projectOrder.buildGraphForProjects(projects,dependencies);
        Stack<String> result = new Stack<>();
        projectOrder.getBuildOrder(result);

    }

    public void buildGraphForProjects(String []projects,String [][]dependencies){
        graph = new Graph();
        for(String project:projects){
            graph.createOrGetNode(project);
        }

        //Add dependencies
        for(String s[] : dependencies){
            String nodeParent = s[0];
            String nodeChild = s[1];
            graph.addChildren(nodeParent,nodeChild);
        }

    }

    public void getBuildOrder(Stack<String> result){
        List<Project> projects = graph.getNodes();
        //topoligical sort here
        for(Project project:projects){
            if(!doDFS(result,project)){
               result = null;
            }
        }
    }

    public boolean doDFS(Stack<String> result,Project project){
        if(project.getState() == State.PARTIAL){
            return false;
        }

        project.setState(State.PARTIAL);
        for(Project child:project.getChildren()){
            if(!doDFS(result,child))
                return  false;
        }
        project.setState(State.COMPLETE);
        result.push(project.getName());
        return true;
    }

}



