package graphs.TopologicalSort;

/**
 * Created by nirmohan on 8/6/17.
 */
public enum State {
        INITIAL,
        PARTIAL,
        COMPLETE;
}
