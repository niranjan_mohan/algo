package graphs.TopologicalSort;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Graph{
    List<Project> nodes = new ArrayList<>();
    Map<String,Project> map = new HashMap<>();

    public Project createOrGetNode(String projectName){
        if(map.containsKey(projectName)){
            return map.get(projectName);
        }
        Project project = new Project(projectName);
        map.put(projectName,project);
        return project;
    }

    public void addChildren(String parentProject,String childProject){
        if(map.containsKey(parentProject) && map.containsKey(childProject)){
            Project parentNode = map.get(parentProject);
            Project childNode = map.get(childProject);
            parentNode.addChildren(childNode);
        }
    }

    public List getNodes() {
        return this.nodes;
    }
}