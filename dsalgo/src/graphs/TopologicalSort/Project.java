package graphs.TopologicalSort;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Project{
    String name;
    Map<String,Project> childMap = new HashMap<>();
    List<Project> children;
    State state;
    Project(String name){
        this.name = name;
        this.state = State.INITIAL;
    }
    public String getName(){
        return this.name;
    }

    public void addChildren(Project childNode){
        childMap.put(childNode.getName(),childNode);
        children.add(childNode);
    }
    public State getState(){
        return this.state;
    }

    public List<Project> getChildren(){
        return this.children;
    }

    public void setState(State state){
        this.state = state;
    }

}