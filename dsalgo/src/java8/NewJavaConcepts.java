package java8;

/**
 * Created by niranjan on 5/14/17.
 */
public class NewJavaConcepts {


    //Thread local start (Since JDK 1)

    private static final ThreadLocal<Object> permissions=
            new ThreadLocal<Object>(){
                @Override
                public Integer initialValue() {
                    return 1;
                }
            };


    public static Object getPermissions(){
        return permissions;
    }
    //Thread local end


    // Lazy loading with thread safe . For active loading use double checked idiom with volatile
//    This relies on the fact that nested classes are not loaded until they are referenced.
    private static class Holder{
        public static final NewJavaConcepts instance = new NewJavaConcepts();
    }

    //prvate class is initialized only if this function is called by outer class eg.
    // NewJavaConcepts instance = NewJavaConcepts.getLazyInstance();
    public static NewJavaConcepts getLazyInstance(){
        return Holder.instance;
    }


//
//    Note the local variable result, which seems unnecessary.
//    The effect of this is that in cases where helper is already initialized (i.e., most of the time),
//    the volatile field is only accessed once (due to "return result;" instead of "return helper;"),
//    which can improve the method's overall performance by as much as 25 percent.

    private volatile  NewJavaConcepts instance;

    public NewJavaConcepts getInstance(){
        NewJavaConcepts result = instance;
        if(result ==null){
            synchronized (this){
                result = instance;
                if(result == null){
                    instance = new NewJavaConcepts();
                }
            }
        }
        return result;
    }


    //END


    //Without using final how to get singleton wth thread safety

    public class FinalWrapper<T>{
        public T value;
        public FinalWrapper(T value){
            this.value = value;
        }
    }

    FinalWrapper<NewJavaConcepts> finalWrapper;

    public  NewJavaConcepts getInstanceWithoutVolatile(){
        FinalWrapper<NewJavaConcepts> temp =finalWrapper ;
        if(temp == null){
            synchronized(this){
                if(temp == null){
                    finalWrapper = new FinalWrapper<>(new NewJavaConcepts());
                }
                temp = finalWrapper;
            }//end for sync block
        }
        return temp.value;
    }
}
