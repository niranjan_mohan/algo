package algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nirmohan on 9/11/17.
 */
public class app {

    public static void main(String []args){
        String [] sIps = {"198.123.11.10","198.123.11.10","197.123.11.10","196.211.11.10","196.111.11.10","196.123.131.10","196.123.11.10","195.123.11.10","194.123.11.10",
                "10.123.11.0","10.123.11.1","10.123.2.10","10.123.3.10","10.123.11.10","198.123.11.10"};


//        String [] sIps = {"0.0.0.1","0.0.0.1","0.0.0.2","0.0.0.3"};
        List<String> ipLists = Arrays.asList(sIps);
        System.out.println("unque ips ::"+findUniqueIps(ipLists).toString());

        Node node  =  new Node();
        node.addNode(5);node.addNode(2);node.addNode(8);node.addNode(3);node.addNode(6);node.addNode(9);node.addNode(1);
        node.printBST();
//
        System.out.println("ABC notation :"+node.getABCN(6));
        System.out.println("ABC notation :"+node.getABCN(1));
        System.out.println("ABC notation :"+node.getABCN(10));
        System.out.println("ABC notation :"+node.getABCN(2));

        System.out.println("************** mean **************");
        int  [] arr = {1,2,3,4,5,6,7,8,9,10};
        calculateMean(arr);

    }


    // find the number of unique IPS in a log file

    public static List<String> findUniqueIps(List<String> ips){
        int size = (int) (Math.pow(2,32) / 64) ;
        //this is same as calculating size
        int s2 = 1<<(32-6);
        System.out.println(" My size :"+size+"their size"+ s2);


        long []memo = new long[size];
        long []dups = new long[size];

        for(String s:ips){
            long  longIp = getLongIp(s);

            int pos = (int)(longIp /64);
            int digit =(int)(longIp%64);

            //check the map for the value
            if( (memo[pos] >> digit) ==1 ){
                //value exists add to dups;
                dups[pos] |= (1 <<digit);
            }else{
                memo[pos] |= (1 <<digit);
            }
        }

        // see which ips are present in dups don't add those to the result
        List<String> result = new ArrayList<String>();
        for(String s:ips){
            long  longIp = getLongIp(s);

            int pos = (int)(longIp /64);
            int digit =(int)(longIp%64);

            if((dups[pos] >> digit) ==0){
                result.add(s);
            }
        }
        return result;
    }

    private static Long getLongIp(String s){
        Long longIp = 0l;
        String [] sarr = s.split("\\.");
        for(int i=0;i<sarr.length;i++) {
            //convert s1 from base 64 to base 10
            int power = 3 - i;
            int ip = Integer.parseInt(sarr[i]);
            longIp += (long)(ip * Math.pow(256, power));
        }
//        System.out.println("s   :"+s+"IPS :"+longIp);
        return longIp;
    }


    //ABC notation for a tree


    //find the average of a very large stream use below formula
    /*
    avg = nx /n+1+  m/n+1 here n = no of elements X =avg of n numbers  m -> new number (n+1)th number
    cannot use int for this calculation as n/n+1 will loose precision

    */

    public static void calculateMean(int []arr){
        int total = arr.length;


        int sum =0;
        for(int i=0;i<arr.length;i++){
            sum+= arr[i];
        }

        //running avg
        int avg =sum/(arr.length);
        System.out.println("avg :"+avg);



        int tempavg = (arr[0] + arr[1])/2;
        System.out.println("temp avg :"+tempavg);

        double runningAvg = (double)tempavg;
        //calculate values from stream
        for(int i=2;i<arr.length;i++){
            runningAvg += ((runningAvg-arr[i] )/i);
        }

        System.out.println("running avg :"+runningAvg);

    }


    /*  Given an array A and some queries, query(i, j) returns the result of Ai*...*Aj, in other words the multiplication from Ai to Aj.
    The numbers in A are non-negative.
            Implement query(i, j).*/


    public void createMemoMap(int []arr){
        int []memo = new int[arr.length];
        int prev =1;
        for(int i=0;i<arr.length;i++){
            if(arr[i]!=0){
                memo[i] = arr[i]*prev;
                prev = memo[i];
            }else{
                memo[i] =0;
                prev =1;
            }
        }
    }

    public int query(int i,int j,int [] memo){
        if(i <0 || j >= memo.length){
            return  -1;
        }

        if(i==0) return memo[j];

        else return memo[j]/memo[i-1];


    }









}
