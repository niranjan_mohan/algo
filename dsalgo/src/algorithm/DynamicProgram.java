package algorithm;

import java.util.*;

import javax.lang.model.type.PrimitiveType;
import javax.swing.tree.ExpandVetoException;

public class DynamicProgram {

	public static void main(String [] args){
		int no = 2567;

		seivePrime(100);
		printPrimePair(24);
		String s = "abcba";
//		System.out.println("sub palindrome  :"+longestPalindrome(s));
		printPalindromes(s);
//		System.out.println("Longest palindrome :"+LPS(s));
//		lcs("agcat","gac");
//
//		int [] arr ={3,2,6,4,5,1,9,6,7};
//		getLIS(arr);
//
//		System.out.println("get the LIS in grid");
//		int[][] grid = {{8, 2, 4},
//				{0, 7, 1},
//				{3, 7, 9}};
//		System.out.println(findGridLIS(grid));
//
//
		System.out.println("Word break");
		String str = "catsanddog";
		Set<String> dict = new HashSet<>();
		dict.add("go");dict.add("goal");dict.add("goals");dict.add("special");dict.add("cat");
		dict.add("and");dict.add("dog");dict.add("cats");dict.add("sand");
		System.out.println("Is word break :"+wordBreak(str,dict));
		List <String>ls = getWordBreaks(str,dict);
		for(String r:ls)
			System.out.println("Word :"+r);

//		kandanes(arr);
		knapSack();

		System.out.println("Min distance b/w words "+minDistance("word","bird"));

		System.out.println("Longest palindrome is "+subPalindrome("abcba"));


	}



	//Knap sack problem
	public static int knapSack(){
		int wt [] = {2,3,1};
		int val[] = {2,5,4};
		int W = 5;
		int maxValue  = knapSack(wt,val,W);
		return maxValue;

	}
	public static int knapSack(int []wt,int []val,int W){
		if (wt ==null || val ==null){
			return -1;
		}
		int [][] kp = new int[val.length+1][W+1];
		for(int b=0;b<=val.length;b++){
			for(int w=0;w<=W;w++){
				if(b == 0 || w ==0){
					kp[b][w] = 0;
				}
				else if(wt[b-1] <= w){
					kp[b][w] = Math.max(val[b-1] +kp[b-1][w-wt[b-1]],kp[b-1][w]);
				}
				else{
					kp[b][w] = kp[b-1][w];
				}
			}
		}
		System.out.println(" Knapsack max value is"+kp[val.length][W]);
		return kp[val.length][W];
	}



	//**********************Find if a number is prime or not given a range ***************************


	//sieve eratosthenes
	public static void seivePrime(int range){
		Boolean [] isPrime = new Boolean[range];
		for(int i=0;i<range;i++){
			isPrime[i] = true;
		}
		for(int i=2;i*i < range ;i++){
			if(isPrime[i]){
				for(int j=i;i*j< range ;j++){
					isPrime[j*i] = false;
				}
			}
		}
		//System.out.println(isPrime.length);
		for(int k=2;k<range;k++){
			if(isPrime[k])
				System.out.print(k);
		}
	}



	//modify the code above to Goldbach's conjecture	
	public static void printPrimePair(int no){
		//find all the primes eratosthenes algo
		Boolean [] isPrime = new Boolean[no+1];
		for(int i=0;i<no;i++){
			isPrime[i]= true;
		}
		Set<Integer> primeSet = new HashSet<Integer>();
		for(int i=2;i*i<no;i++){
			if(isPrime[i]){
				for(int j=i;j*i<no;j++){
					isPrime[j*i] = false;
				}
			}
		}
		for(int i=2;i<no;i++){
			if(isPrime[i])
				primeSet.add(i);
		}
		//get the goldbach's conjucture 
		for(Integer i:primeSet){
			if(primeSet.contains(no-i)){
				System.out.println("the pair :"+i+" "+(no-i));
				break;
			}
		}
	}


	public static void lcs(String s1,String s2){
		//intialize the 2D array to 0

		int len1 = s1.length();
		int len2 = s2.length();
		int length[][] = new int[len1+1][len2+1];

		//Note: this is not required as int[][] is auto initialized to 0's
//		for(int i=0;i<len1+1;i++)
//			for(int j=0;j<len2+1;j++)
//				length[i][j] =0;

		//code for the lcs
		for(int i=0;i<len1;i++)
			for(int j=0;j<len2;j++)
				if(s1.charAt(i) == s2.charAt(j))
					length[i+1][j+1] = 1+ length[i][j];
				else 
					length[i+1][j+1] = Math.max(length[i+1][j],length[i][j+1]);


		StringBuilder stb = new StringBuilder();		
		for(int x = len1, y= len2 ;x !=0 && y!=0;){
			if(length[x][y] == length[x-1][y])
				x--;
			else if(length[x][y] == length[x][y-1])
				y--;
			else{
				//assert(s1.charAt(x) == s2.charAt(y));
				stb.append(s1.charAt(x-1));
				x--;y--;
			}
		}
		stb.reverse();
		System.out.println("the Lcs is "+stb.toString());
	}




	public static void printPalindromes(String str){
		int len = str.length();
		List<String> palList = new ArrayList<String>();

		//print the odd palindromes

		for(int i=1;i<len-1;i++){
			for(int j=i+1,k=i-1; i<len && k >=0; j++, k-- ){
				if(str.charAt(j) == str.charAt(k)){
					if (j-k+1 >= 3)
					System.out.println("odd print pal "+ str.substring(k,j+1));
				}
				else{
					break;
				}
			}
		}
		//print even
		for(int i=1;i<len-1;i++){
			for(int j=i,k=i-1; i<len && k >=0; j++, k-- ){
				if(str.charAt(j) == str.charAt(k)){
					if (j-k+1 >= 3)
					System.out.println("even print pal "+str.substring(k,j+1));
				}
				else{
					break;
				}
			}
		}
	}

	//TODO : this is matrix method with 0(n^2)

	public static String subPalindrome(String s){
		int len = s.length();
		String result = null;
		char [] sarr = s.toCharArray();
		int arr[][] = new int[len][len];
		for(int i=0;i<len;i++){
			arr[i][i] = 1;
		}
		for(int i=0;i<len-1;i++){
			if(sarr[i] == sarr[i+1]){
				arr[i][i+1] = 1;
			}
			else{
				arr[i][i+1] =0;
			}
		}
		for(int i=2;i<len;i++)
			for(int j=0;j+i<len;j++){
				int k = i+j;
				if(sarr[j] == sarr[k] && arr[j+1][k-1] == 1){
					arr[j][k] = 1;
					result = s.substring(j,k+1);
				}
				else
					arr[j][k] =0;
			}
		return result;
	}










	// word break need test
		/*
	 given s = "catsanddog",
	 dict = ["cat", "cats", "and", "sand", "dog"]
	 solution =  true  // explanation 4 distinct words are possible ["cats and dog", "cat sand dog"].
	 */

	public static boolean wordBreak(String str,Set<String> dict){
		int len =str.length();

		boolean bit[] = new boolean[len+1];
		bit[0] = true;
		for(int i=0;i< len;i++){
			if(!bit[i])
				continue;
			for(String s:dict){
				int end = s.length()+i;
				if(len < end)
					continue;
				if(bit[end])
					continue;
				if(str.substring(i,end).equals(s))
					bit[end] = true;
			}

		}
		return bit[str.length()];
	}




	//todo : what is wrong with this see explanation below
	// this will not work for catsanddogs -> cats and dogs

	public boolean isWordBreak(String str, Set<String> dict){
		int len = str.length();
		for(int i=0;i<len;i++){
			String prefix =str.substring(0,i);
			if(dict.contains(prefix)){
				String sufix = str.substring(i,len);
				if(dict.contains(sufix))
					//return prefix + sufix
					return true;
			}
		}
		return false;
	}
// End of wrong code

	/*
	 given s = "catsanddog",
	 dict = ["cat", "cats", "and", "sand", "dog"]
	 solution =  ["cats and dog", "cat sand dog"].
	 */

	//TODO : word break 2 algo displays all the words seperated




	public static List<String> getWordBreaks(String str, Set<String> dict){
		int strlen = str.length();
		List [] dp = new ArrayList[strlen+1];
		dp[0] = new ArrayList<String>();
		for(int i=0;i<strlen;i++){
			if(dp[i] ==null)
				continue;
			for(String s:dict){
				int end = s.length()+i;
				if(end > strlen)
					continue;
				if(str.substring(i,end).equals(s)){
					if(dp[end] == null)
						dp[end] = new ArrayList<>();
					dp[end].add(s);
				}
			}
		}
		List<String> result = new LinkedList<String>();
		if(dp[strlen] == null)
			return result;

		ArrayList<String> temp = new ArrayList<String>();
		dfs(dp, strlen, result, temp);

		return result;
	}

	public static void dfs(List<String> dp[],int end,List<String> result, ArrayList<String> tmp){
		if(end <= 0){
			String path = tmp.get(tmp.size()-1);
			for(int i=tmp.size()-2; i>=0; i--){
				path += " " + tmp.get(i) ;
			}
			result.add(path);
			return;
		}

		for(String str : dp[end]){
			tmp.add(str);
			dfs(dp, end-str.length(), result, tmp);
			tmp.remove(tmp.size()-1);
		}
	}



	//******************************* End of word break 2 *******************************

	//find the longest increasing sub-sequence
	public static void getLIS(int A[]){
		int len = A.length;
		int memo[] = new int[len];
		int dp[] = new int[len];
		int end =0;
		int maxlen =1;
		memo[0] = -1;
		dp[0] =1;
		for(int i=1;i<len;i++){
			memo[i] = -1;
			dp[i] = 1;
			for(int j=i-1;j>=0;j--){
				if(dp[i] < dp[j]+1 && A[i] > A[j]){
					dp[i] = dp[j]+1;
					memo[i] = j;//this is the start point;
				}
				if(maxlen < dp[i]){
					maxlen = dp[i];
					end = i ;
				}
			}
		}
		//print the LIS
		System.out.println("length of MIS "+ maxlen+ "-----");
		System.out.println();
		List<Integer> list = new ArrayList<Integer>();
		list.add(A[end]);
		//System.out.print(A[end]+" ");
		while(memo[end] !=-1 ){
			list.add(A[memo[end]]);
			end = memo[end];
		}
		Collections.reverse(list);
		System.out.println(list.toString());
	}




	//Find the LIS in a grid 


	/* 
	 * 8	2	4
	 * 0	7	1
	 * 3	7	9
	 */



	public static int findGridLIS(int A[][]){
		int len = A.length;
		int dp [][] = new int [len][len];
		int max =0;
		//intialize memoizing array with all ones

		for(int i=0;i<len;i++){
			for(int j=0;j<len;j++){
				int tempmax = fillDP(A,dp,i,j)+1;
				if(tempmax > max)
					max = tempmax;
			}
		}
		return max;

	}

	public static int fillDP(int [][]A,int[][]dp,int i,int j){
		int len = A.length;
		if(i <0 || j < 0 || i >= len || j >= len ){
			return 0;
		}
		if(dp[i][j] != 0)
			return dp[i][j];
		int left=0,right=0,topLeft=0,topRight=0,bottom=0,top=0,bottomLeft=0,bottomRight=0;
		if(j-1 >= 0&& A[i][j] < A[i][j-1])
			left = fillDP(A,dp,i,j-1);

		if(j+1<len && A[i][j] < A[i][j+1])
			right = fillDP(A,dp,i,j+1);

		if(i+1 <len && A[i][j] < A[i+1][j])
			bottom = fillDP(A,dp,i+1,j);

		if(i-1 >=0 && A[i][j] < A[i-1][j])
			top = fillDP(A,dp,i-1,j);

		if(i+1 < len && j-1 >=0 && A[i][j] < A[i+1][j-1])
			bottomLeft = fillDP(A,dp,i+1,j-1);

		if(i+1 >len && j+1 >len && A[i][j] < A[i+1][j+1])
			bottomRight = fillDP(A,dp,i+1,j+1);

		if(i-1 >=0 && j-1 >=0 && A[i][j] < A[i-1][j-1])
			topLeft = fillDP(A,dp,i-1,j-1);

		if(i-1 >=0 && j+1 <len && A[i][j] < A[i-1][j+1])
			topRight = fillDP(A,dp,i-1,j+1);

		dp[i][j]= Math.max(Math.max(Math.max(top,bottom),Math.max(right,left)),Math.max(Math.max(bottomRight,bottomLeft),Math.max(topLeft,topRight))) +1;
		return dp[i][j]; 

	}



	//TODO : missing corner case manachers algorithm run in 0(n) time




	//kandanes algorithm

	public static Integer kandanes(int [] arr){
		int local_max,global_max;
		int start=0,end=0,start_untilnow=0;
		local_max = global_max = arr[0];
		for(int i=0;i<arr.length;i++){
			local_max+= arr[i];
			if(local_max < arr[i]){
				local_max = arr[i];
				start_untilnow = i;
			}else{
				end = i;
			}
			if(global_max < local_max)
				global_max = local_max;
			start = start_untilnow;
		}
		System.out.println("Start :"+start+" End :"+end);
		return global_max;
	}




	/**
	 * Rod cutting problem
	 * Given a rod of length n inches and an array of prices that contains prices of all pieces of size smaller than n.
	 * Determine the maximum value obtainable by cutting up the rod and selling the pieces.
	 *
	  length   | 1   2   3   4   5   6   7   8
	 --------------------------------------------
	 price    | 1   5   8   9  10  17  17  20
	 */


	public int lrodCutting(int []values,int rodLen){
		int memo[] = new int[values.length+1];
		memo[0]=0;
		for(int i=1;i<=rodLen;i++){
			int maxVal = 0;
			for(int j=0;j<i;j++){
				maxVal = Math.max(maxVal,values[j]+memo[i-j-1]);
			}
			memo[i] = maxVal;
		}

		return memo[rodLen];
	}


	/**
	 * unbounded knapsack problem
	 *
	 */


	/**
	 *
	 * In a daily share trading, a buyer buys shares in the morning and sells it on same day.
	 * If the trader is allowed to make at most 2 transactions in a day, where as second transaction
	 * can only start after first one is complete (Sell->buy->sell->buy). Given stock prices throughout day,
	 * find out maximum profit that a share trader could have made.

	 Examples:

	 Input:   price[] = {10, 22, 5, 75, 65, 80}
	 Output:  87
	 Trader earns 87 as sum of 12 and 75
	 Buy at price 10, sell at 22, buy at 5 and sell at 80

	 Input:   price[] = {2, 30, 15, 10, 8, 25, 80}
	 Output:  100
	 Trader earns 100 as sum of 28 and 72
	 Buy at price 2, sell at 30, buy at 8 and sell at 80

	 Input:   price[] = {100, 30, 15, 10, 8, 25, 80};
	 Output:  72
	 Buy at price 8 and sell at 80.

	 Input:   price[] = {90, 80, 70, 60, 50}
	 Output:  0
	 Not possible to earn.
	 *
	 *
	 */

	public int maxProfit(int [] price){
		// Create profit array and initialize it as 0
		int n = price.length;
		int profit[] = new int[n];
		for (int i=0; i<n; i++)
			profit[i] = 0;

        /* Get the maximum profit with only one transaction
           allowed. After this loop, profit[i] contains maximum
           profit from price[i..n-1] using at most one trans. */
		int max_price = price[n-1];
		for (int i=n-2;i>=0;i--)
		{
			// max_price has maximum of price[i..n-1]
			if (price[i] > max_price)
				max_price = price[i];

			// we can get profit[i] by taking maximum of:
			// a) previous maximum, i.e., profit[i+1]
			// b) profit by buying at price[i] and selling at
			//    max_price
			profit[i] = Math.max(profit[i+1], max_price-price[i]);
		}

        /* Get the maximum profit with two transactions allowed
           After this loop, profit[n-1] contains the result */
		int min_price = price[0];
		for (int i=1; i<n; i++)
		{
			// min_price is minimum price in price[0..i]
			if (price[i] < min_price)
				min_price = price[i];

			// Maximum profit is maximum of:
			// a) previous maximum, i.e., profit[i-1]
			// b) (Buy, Sell) at (min_price, price[i]) and add
			//    profit of other trans. stored in profit[i]
			profit[i] = Math.max(profit[i-1], profit[i] +
					(price[i]-min_price) );
		}
		int result = profit[n-1];
		return result;

	}


	/**
	 * levenstein edit distance
	 *
	 *  possible operations : add delete replace
	 *
	 */

	public int levensteinEditDistance(String s1,String s2){
		if(s1 == null || s2 ==null){
			return -1;
		}
		if(s1.equals(s2)){
			return 0;
		}
		int len1 = s1.length();
		int len2 = s2.length();
		int [][]memo = new int[len1-1][len2-1];
		for(int i=0;i<len1;i++)
			for(int j=0;j<len2;j++)
				memo[i][j]=-1;

		return compare(s1,s2,len1-1,len2-1,memo);

	}

	public  static int compare(String s1,String s2,int index1,int index2,int [][]memo){
		if(index1 <0 && index2 <0)  return 0;

		if(index1 < 0) return index2;

		if(index2 < 0 ) return index1;

		if(memo[index1][index2] != -1){
			return memo[index1][index2];
		}
		if(s1.charAt(index1) == s2.charAt(index2)){
			memo[index1][index2] =  compare(s1,s2,index1-1,index2-1,memo);
			return memo[index1][index2];
		}
		memo[index1][index2]=1+ Math.min(Math.min(compare(s1,s2,index1-1,index2,memo),compare(s1,s2,index1,index2-1,memo)),compare(s1,s2,index1-1,index2-1,memo));
		return memo[index1][index2];
	}

	/**
	 *  in place version
	 *   runtime O(nm)
	 *   space O(nm)
	 */
	public int dynamicEditDistance(char[] str1, char[] str2){
		int temp[][] = new int[str1.length+1][str2.length+1];

		for(int i=0; i < temp[0].length; i++){
			temp[0][i] = i;
		}

		for(int i=0; i < temp.length; i++){
			temp[i][0] = i;
		}

		for(int i=1;i <=str1.length; i++){
			for(int j=1; j <= str2.length; j++){
				if(str1[i-1] == str2[j-1]){
					temp[i][j] = temp[i-1][j-1];
				}else{
					temp[i][j] = 1 + min(temp[i-1][j-1], temp[i-1][j], temp[i][j-1]);
				}
			}
		}
//		printActualEdits(temp, str1, str2);
		return temp[str1.length][str2.length];

	}

	private int min(int a,int b, int c){
		int l = Math.min(a, b);
		return Math.min(l, c);
	}



	/**
	 *
	 * runtime O(nm)
	 * space O(n)
	 *  (variation Hirschberg's algorithm w/o Divide and conquer)
	 */

	public static int minDistance(String str1, String str2) {
		int m = str1.length();
		int n = str2.length();
		// Create a table to store results of subproblems
		// To process a row, we just need the previous rows value. So 1d array should be enough.
		int dp[]= new int[n+1];

		//For first row m=0(first string is empty), we have to insert all the char in second string.
		for(int i=0;i<=n;i++){
			dp[i] = i;
		}
		// Fill d[][] in bottom up manner
		for (int i=1; i<=m; i++)
		{
			// Prev will hold the previous row's previous column.
			// This is replacement for dp[i-1][j-1] in two dp;
			// Take first value for  i-1 th row.
			int prev = dp[0];
			// If second string is empty, only option is to
			// remove all characters of first  string
			dp[0] = i;
			for (int j=1; j<=n; j++)
			{
				int t = 0;
				// If last characters are same, ignore last char
				// and recur for remaining string
				if (str1.charAt(i-1) == str2.charAt(j-1))
					//dp[i][j] = dp[i-1][j-1];
					t = prev;

					// If last character are different, consider all
					// possibilities and find minimum
				else
					t = 1 + Math.min(Math.min(dp[j-1],  // Insert
							dp[j]) , // Remove
							prev); // Replace

				prev = dp[j];
				dp[j] = t;
			}
		}

		return dp[n];
	}




 /*
 * Given stockc prices for certain days and at most k transactions how to buy and sell
 * to maximize profit.
 *
 * Time complexity - O(number of transactions * number of days)
 * Space complexity - O(number of transcations * number of days)
 *
 *
 * https://leetcode.com/discuss/15153/a-clean-dp-solution-which-generalizes-to-k-transactions
 */



	public int maxProfitLinearSpace(int k, int[] prices) {
		if (k == 0 || prices.length == 0) {
			return 0;
		}

		if (k >= prices.length) {
			return allTimeProfit(prices);
		}
		int[] T = new int[prices.length];
		int[] prev = new int[prices.length];
		for (int i = 1; i <= k; i++) {
			int maxDiff = -prices[0];
			for (int j = 1; j < prices.length; j++) {
				T[j] = Math.max(T[j - 1], maxDiff + prices[j]);
				maxDiff = Math.max(maxDiff, prev[j] - prices[j]);
			}
			for (int j = 1; j < prices.length; j++) {
				prev[j] = T[j];
			}
		}
		return T[T.length - 1];
	}

	public int allTimeProfit(int arr[]){
		int profit = 0;
		int localMin = arr[0];
		for(int i=1; i < arr.length;i++){
			if(arr[i-1] >= arr[i]){
				localMin = arr[i];
			}else{
				profit += arr[i] - localMin;
				localMin = arr[i];
			}

		}
		return profit;
	}

	/**
	 * This is faster method which does optimization on slower method
	 * Time complexity here is O(K * number of days)
	 *
	 * Formula is
	 * T[i][j] = max(T[i][j-1], prices[j] + maxDiff)
	 * maxDiff = max(maxDiff, T[i-1][j] - prices[j])
	 */
	public int maxProfit(int prices[], int K) {
		if (K == 0 || prices.length == 0) {
			return 0;
		}
		int T[][] = new int[K+1][prices.length];

		for (int i = 1; i < T.length; i++) {
			int maxDiff = -prices[0];
			for (int j = 1; j < T[0].length; j++) {
				T[i][j] = Math.max(T[i][j-1], prices[j] + maxDiff);
				maxDiff = Math.max(maxDiff, T[i-1][j] - prices[j]);
			}
		}
		printActualSolution(T, prices);
		return T[K][prices.length-1];
	}

	/**
	 * This is slow method but easier to understand.
	 * Time complexity is O(k * number of days ^ 2)
	 * T[i][j] = max(T[i][j-1], max(prices[j] - prices[m] + T[i-1][m])) where m is 0...j-1
	 */
	public int maxProfitSlowSolution(int prices[], int K) {
		if (K == 0 || prices.length == 0) {
			return 0;
		}
		int T[][] = new int[K+1][prices.length];

		for (int i = 1; i < K+1; i++) {
			for (int j = 1; j < prices.length; j++) {
				int maxVal = 0;
				for (int m = 0; m < j; m++) {
					maxVal = Math.max(maxVal, prices[j] - prices[m] + T[i-1][m]);
				}
				T[i][j] = Math.max(T[i][j-1], maxVal);
			}
		}
		printActualSolution(T, prices);
		return T[K][prices.length - 1];
	}

	public void printActualSolution(int T[][], int prices[]) {
		int i = T.length - 1;
		int j = T[0].length - 1;

		Deque<Integer> stack = new LinkedList<>();
		while(true) {
			if(i == 0 || j == 0) {
				break;
			}
			if (T[i][j] == T[i][j-1]) {
				j = j - 1;
			} else {
				stack.addFirst(j);
				int maxDiff = T[i][j] - prices[j];
				for (int k = j-1; k >= 0; k--) {
					if (T[i-1][k] - prices[k] == maxDiff) {
						i = i - 1;
						j = k;
						stack.addFirst(j);
						break;
					}
				}
			}
		}

		while(!stack.isEmpty()) {
			System.out.println("Buy at price " + prices[stack.pollFirst()]);
			System.out.println("Sell at price " + prices[stack.pollFirst()]);
		}

	}

	/**
	 *
	 *
	 * Given a 2D matrix matrix, find the sum of the elements inside the rectangle defined by its upper left corner (row1, col1) and lower right corner (row2, col2).

	 Range Sum Query 2D
	 The above rectangle (with the red border) is defined by (row1, col1) = (2, 1) and (row2, col2) = (4, 3), which contains sum = 8.

	 Given matrix = [
	 [3, 0, 1, 4, 2],
	 [5, 6, 3, 2, 1],
	 [1, 2, 0, 1, 5],
	 [4, 1, 0, 1, 7],
	 [1, 0, 3, 0, 5]
	 ]

	 sumRegion(2, 1, 4, 3) -> 8
	 sumRegion(1, 1, 2, 2) -> 11
	 sumRegion(1, 2, 2, 4) -> 12
	 *
	 *
	 */



	private int[][] dp;

	public void NumMatrix(int[][] matrix) {
		if (matrix.length == 0 || matrix[0].length == 0) return;
		dp = new int[matrix.length + 1][matrix[0].length + 1];
		for (int r = 0; r < matrix.length; r++) {
			for (int c = 0; c < matrix[0].length; c++) {
				dp[r + 1][c + 1] = dp[r + 1][c] + dp[r][c + 1] + matrix[r][c] - dp[r][c];
			}
		}
	}

	public int sumRegion(int row1, int col1, int row2, int col2) {
		return dp[row2 + 1][col2 + 1] - dp[row1][col2 + 1] - dp[row2 + 1][col1] + dp[row1][col1];
	}


}



