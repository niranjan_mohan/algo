package algorithm;

/**
 * Created by nirmohan on 9/12/17.
 */
class Node {
    Node left;
    Node right;
    int value;
    Node(int value){
        this.value = value;
    }
    Node(){

    }


    Node rootNode;



    public Node getRootNode(){
        return this.rootNode;
    }


    public void addNode(int no){
        if(rootNode ==null){
            rootNode = new Node(no);
            return;
        }
        Node node = rootNode;
        Node parent = null;
        boolean isLeftNode = true;
        while(node !=null) {
            parent = node;
            if (node.value > no) {
                isLeftNode = true;
                node = node.left;
            }else{
                isLeftNode = false;
                node = node.right;
            }
        }
        if(isLeftNode){
           parent.left = new Node(no);
        }else{
            parent.right = new Node(no);
        }
    }

    public void printBST(){
        Node n = rootNode;
        printInOrder(n);
    }

    private void printInOrder(Node node){
        if(node == null){
            return;
        }
        printInOrder(node.left);
        System.out.println(node.value);
        printInOrder(node.right);
    }

    public String getABCN(int no){
        if(rootNode == null){
            return "Not Found";
        }
        if(rootNode.value == no){
            return "Undefined";
        }
        else{
            Node n = rootNode;
            StringBuilder stb = new StringBuilder();
            getABCNotation(n,no,stb);
            return stb.toString();
        }
    }

    public void getABCNotation(Node n, int no, StringBuilder stb){
        if(n == null){
            stb.delete(0,stb.length());
            stb.append("Not Found");
            return;
        }
        if(n.value == no ){
            return ;
        }else{
            if(n.value > no){
                stb.append("0");
                getABCNotation(n.left,no,stb);
            }else   {
                stb.append("1");
                getABCNotation(n.right,no,stb);
            }
        }
    }





}
