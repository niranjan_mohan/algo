package algorithm.sort;

/**
 * Created by nirmohan on 7/16/17.
 */
public class LinkedListMergeSort {


    public Node mergeSortLinkedList(Node node){
        if(node == null || node.next ==null) {
            return node;
        }

        Node middleNode = getMiddleNode(node);
        Node nextToMiddle = middleNode.next;

        //end the first middle;
        middleNode.next =null;

        //recurse
        Node left = mergeSortLinkedList(middleNode);
        Node right = mergeSortLinkedList(nextToMiddle);

        Node result = mergeLinkedLists(left,right);
        return result;
    }



    Node mergeLinkedLists(Node left,Node right){
        Node result=null;
        if(left ==null)
            return right;


        if(right ==null)
            return left;

        if(left.data < right.data){
            result = left;
            result.next = mergeLinkedLists(left.next,right);
        }
        else{
            result = right;
            result.next = mergeLinkedLists(left,right.next);
        }

        return result;
    }




    Node getMiddleNode(Node node){
        Node slow =node;
        Node fast = node;

        while(fast !=null && fast.next !=null){
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }


    public static void main(String [] args){

    }





    class Node{
        Node next;
        int data;
        Node(int data){
            this.data = data;
        }
    }


}
