package algorithm.sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by niranjan on 5/7/17.
 */
public class Generics<T extends Comparable<T>> {

    public static void main(String [] args){
        Generics<Integer> genList = new Generics<>();
        List <Integer> ilist = Arrays.asList(1,5,4,2,0,8);
        List <Integer>randList = new ArrayList<Integer>(ilist);
        List<Integer> result =  genList.mergeSort(randList);
        System.out.println("result is ::"+ result.toString());

        Generics<String> stringGenerics = new Generics<>();
        List <String> slist = Arrays.asList("n","i","r","a","n","j","a","n");
        List <String>srandList = new ArrayList<String>(slist);
        List<String> results =  stringGenerics.mergeSort(srandList);
        System.out.println("result is ::"+ results.toString());
    }

    public List<T> mergeSort(List<T> list){
        if(list ==null || list.size() <=1)
            return list;
        int size = list.size();
        int mid = size/2;
        List<T> result;
        List<T> left = list.subList(0,mid);
        List<T> right = list.subList(mid,size);
        left = mergeSort(left);
        right = mergeSort(right);
        result = merge(left,right);
        return result;
    }


    public List <T> merge(List<T> left, List<T> right){
        if(left == null && right == null)
            return null;
        List<T> result = new ArrayList<>();
        Iterator<T> itl = left.iterator();
        Iterator<T> itr = right.iterator();
        T l = itl.next();
        T r = itr.next();
        while(true){
            if(l.compareTo(r) <1){
                result.add(l);
                if(itl.hasNext())
                    l = itl.next();
                else{
                    result.add(r);
                    while(itr.hasNext())
                        result.add(itr.next());
                    break;
                }
            }
            //else if r <= l
            else{
                result.add(r);
                if(itr.hasNext())
                    r = itr.next();
                else{
                    result.add(l);
                    while(itl.hasNext())
                        result.add(itl.next());
                    break;
                }
            }
        }
        return result;
    }
}
