package algorithm.sort.misc;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Test {
public static void main(String [] args){
	List<Integer>ls = new ArrayList<Integer>();
	List<Integer>right = null;
	List<Integer> left = null;
	for(int i=0;i<2;i++){
		ls.add(i);
	}
	System.out.println(ls.size());
	//System.out.println(ls.get(9));
	left =ls.subList(0, ls.size()/2);
	right = ls.subList(ls.size()/2, ls.size());
	System.out.println("left::"+left.toString()+"right::"+right.toString());
	
	
	System.out.println("testing function calls");
	List <Integer> lis = new ArrayList<Integer>();
	for(int i=0;i<10;i++){
		lis.add(0);
	}
	updateLis(lis);
	for(int i=0;i<10;i++){
		System.out.print(" "+lis.get(i));
	}


	System.out.println("\n integer bytes value :"+Integer.BYTES);


	int  [] test = {3,2,1,4};
	twoSum(test,5);
	
	
}


// 1 ,2,3 ->


	public static int[] twoSum(int[] nums, int target) {
		if (nums == null || nums.length < 2) {
			return new int[2];
		}
		int[] result = new int[2];
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();

		for (int i = 0; i < nums.length; i++) {
			if (map.containsKey(target - nums[i])) {
				int index1 = i;
				int index2 = map.get(target - nums[i]);
				result[0] = (index1 <= index2) ? index1 : index2;
				result[1] = (index1 >= index2) ? index1 : index2;
				return result;
			}
			map.put(nums[i], i);
		}
		return result;
	}

public  static void updateLis(List<Integer> nlist){
	for(int i=0;i<10;i=i+2)
	nlist.set(i, 999);
}

}
