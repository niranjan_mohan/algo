package algorithm;

/**
 * Created by niranjan on 6/7/17.
 */
public class Online2017 {


    public static void main(String[] args) {

        int[] nums1 = {1, 3, 4, 5, 6, 7};
        int[] nums2 = {2, 8, 10};
//        System.out.println(findMedianSortedArrays(nums1, nums2));


        int[] arr = {3, 4, 5, 0, 1, 2};
        System.out.println("found element at index :" + findElement(arr, 3, 0, 5));


        int[] arr1  = { 0, 1, 2,3, 4,5};
        System.out.println("The max in rotated array is :"+findMax(arr1,0,arr1.length-1));

        leftRotateArray(arr1,6,4);


    }

    /*

    Iterative solution is much faster than recursive one in java

     */

    public double findMedianSortedArrays(int[] A, int[] B) {
        int m = A.length;
        int n = B.length;
        if (m > n) { // to ensure m<=n
            int[] temp = A; A = B; B = temp;
            int tmp = m; m = n; n = tmp;
        }
        int iMin = 0, iMax = m, halfLen = (m + n + 1) / 2;
        while (iMin <= iMax) {
            int i = (iMin + iMax) / 2;
            int j = halfLen - i;
            if (i < iMax && B[j-1] > A[i]){
                iMin = iMin + 1; // i is too small
            }
            else if (i > iMin && A[i-1] > B[j]) {
                iMax = iMax - 1; // i is too big
            }
            else { // i is perfect
                int maxLeft = 0;
                if (i == 0) { maxLeft = B[j-1]; }
                else if (j == 0) { maxLeft = A[i-1]; }
                else { maxLeft = Math.max(A[i-1], B[j-1]); }
                if ( (m + n) % 2 == 1 ) { return maxLeft; }

                int minRight = 0;
                if (i == m) { minRight = B[j]; }
                else if (j == n) { minRight = A[i]; }
                else { minRight = Math.min(B[j], A[i]); }

                return (maxLeft + minRight) / 2.0;
            }
        }
        return 0.0;
    }

    public static double findMedianSortedArraysR(int[] nums1, int[] nums2) {
        int[] arr1 = nums1;
        int[] arr2 = nums2;
        int k = (arr1.length + arr2.length) / 2;
        if (k % 2 == 0) {
            return findMedian(arr1, arr2, 0, 0, k / 2) + findMedian(arr1, arr2, 0, 0, k / 2 + 1);
        }
        return findMedian(arr1, arr2, 0, 0, k / 2 + 1);
    }

    public static int findMedian(int[] arr1, int[] arr2, int s1, int s2, int k) {


        if (s1 >= arr1.length)
            return arr2[s2 + k - 1];

        if (s2 >= arr2.length)
            return arr1[s1 + k - 1];

        if (k == 1)
            return Math.min(arr1[s1], arr2[s2]);


        int mid1 = k / 2 + s1 - 1;
        int mid2 = k / 2 + s2 - 1;

        int m1 = mid1 < arr1.length ? arr1[mid1] : Integer.MAX_VALUE;
        int m2 = mid2 < arr2.length ? arr2[mid2] : Integer.MAX_VALUE;

        if (m1 > m2)
            return findMedian(arr1, arr2, s1, mid2 + 1, k - k / 2);
        else
            return findMedian(arr1, arr2, mid1 + 1, s2, k - k / 2);
    }


    /**
     * find element in rotated array
     * cracking the code solution
     * 3 4 5 0 1 2
     */

    public static int findElement(int[] arr, int e, int start, int end) {
        if (start > end)
            return -1;
        if (arr == null)
            return -1;
        int mid = (start + end)/2;

        if (arr[mid] == e)
            return mid;
        // check if array first half is sorted
        if (arr[start] < arr[mid]) {
            if (e < arr[mid] && e >= arr[start])
                return findElement(arr, e, start, mid - 1);
            else {
                return findElement(arr, e, mid + 1, end);
            }
        }
        //  // check if array second half is sorted
        if (arr[mid] < arr[end]) {
            if (e > arr[mid] && e <= arr[end]) {
                return findElement(arr, e, mid + 1, end);
            } else {
                return findElement(arr, e, start, mid - 1);
            }
        }

        // to handle duplicates

//        if (arr[start] == arr[mid]) {
//            if (arr[mid] != arr[end]) {
//                return findElement(arr, e, mid + 1, end);
//            }
//        }
        else {
            int result = findElement(arr, e, start, mid - 1);
            if (result == -1) {
                return findElement(arr, e, mid + 1, end);
            } else
                return result;
        }

//        return -1;
    }

    public static void leftRotateArray(int []a,int n,int k){
        //naive method
        int temp[] = new int[n];
        for(int i=0;i<n;i++){
            int newIndex = ((n+i)-k)%n;
            temp[newIndex] = a[i];
        }
        for(int i=0;i<n;i++)
            System.out.print(temp[i]+" ");
    }






    public static int findMax(int []arr,int start,int end){
        int mid = (start+end)/2;
        if(mid < start || mid > end)
            return Integer.MIN_VALUE;


        //when  we have reached the rotation point
        int prev = start <= (mid-1)? mid-1:arr.length-1 ;
        int next = end >= (mid +1) ? mid+1 : 0;

        if(start == end)
            return arr[start];
        if(arr[mid] > arr[next] && arr[mid] > arr[prev])
            return arr[mid];

        //decide which half the rotation point is
        if(arr[mid] < arr[start]){
            return findMax(arr,start,mid-1);
        }else{
            return findMax(arr,mid+1,end);
        }

    }





    /**
     *   catalan's number or binomial co-efficient O(n)
     *   Used for many purposes :Balanced Parenthesis mountain ranges rooted binary trees
     *  http://www.geometer.org/mathcircles/catalan.pdf
     */
    public long binomialCoeff(int n,int k){
        long res = 1l;
        if(k> n-k){
            k= n-k;
        }
        // Calculate value of [n*(n-1)*---*(n-k+1)] / [k*(k-1)*---*1]
        for(int i=0;i< k;i++){
            res *= (n-i);
            res /= (k-i);
        }
        return res;

    }


    /*

    isomorphic strings  ex : [egg,add] = true  [paper,title] = true
    e = a
    g = d
    hence egg == add


     */
    class Solution {
        public boolean isIsomorphic(String s, String t) {
            int[] map = new int[512];
            for(int i=0;i<s.length();i++) {
                if(map[s.charAt(i)] != map[t.charAt(i)+256]) return false;
                map[s.charAt(i)] = map[t.charAt(i)+256] = i+1;
            }
            return true;
        }
    }











}
