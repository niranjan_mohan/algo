package algorithm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.PriorityQueue;
import java.util.Set;

public class OnlineProb {

	public static void main(String [] args){

		int [] A ={7,-1,-5,-2, 4, -3,0};
		int r = equi(A);
		System.out.println(" The value is :"+ r);
		int [] A1= {1,2,2,3,5,5,7,8,9,9,9,9,9};
		System.out.println("Index :"+ searchBin(A1, 0, A.length-1, 9, -1));
		int no = 1326;
//		System.out.println("is color"+checkColorful(no));

		printOct(891);
		steppingNos(0, 1000);
		System.out.println("*************************");
		getIndexProd(132);
		System.out.println("is MAgic number ::"+isMagicNumber(2396));
		System.out.println("check magic number"+checkMagic(181));
		 isMagic(199);

		int AA[][] = {{1,2,3},{4,5,6},{7,8,9}};
		printSpiralMatrix(AA);

		int []A2 = {2,3,5,6,1};
		System.out.println("rotattion R is :"+rotationSortedArray(A2));
		int [] onesZeroes = {1,1,1,1,0,0,1,1,0,0};
		findZeroOne(onesZeroes);

		//find missing number

		int  [] missinNo = {1,2,3,5,6,7,8};
		System.out.println("Missing number is"+getMissinNumber(missinNo));
	}








	// Equilibrium problem 
	public static int equi( int[] A ) {
		long sum = 0;
		long leftSum = 0;
		for (int i=0;i<A.length;i++){
			leftSum += A[i];
		}
		for (int i=0;i<A.length;i++){
			if (sum==leftSum-A[i]) return i;
			sum+=A[i];
			leftSum-=A[i];
		}
		return -1;
	}



	/*
	Given an array A of N integers we draw N discs in a 2D plane, such that i-th disc has center
	 in (0,i) and a radius A[i]. We say that k-th disc and j-th disc intersect,
	 if k-th and j-th discs have at least one common point.
	 */

	//Disc intersection problem
	public int number_of_disc_intersections ( int[] A ) {
		int overlaps = 0;
		if (A.length<2) return 0;
		PriorityQueue<Integer> leftEdges = new PriorityQueue<Integer>();
		PriorityQueue<Long> rightEdges = new PriorityQueue<Long>();
		for (int i=0; i<A.length; i++){
			leftEdges.add(i-A[i]);
			rightEdges.add((long)i+(long)(A[i]));
		}
		int otherCirclesAtThisEdgeNum = 0;
		while ( !rightEdges.isEmpty()) {
			try {
				if (leftEdges.element() <= rightEdges.peek() ) {
					overlaps += otherCirclesAtThisEdgeNum++;
					if (overlaps > 10000000) return -1;
					leftEdges.poll();
				} else {
					otherCirclesAtThisEdgeNum--;
					rightEdges.poll();
				}
			}catch (NoSuchElementException e){
				break;
			}
		}
		return overlaps;
	}


	//colorful number
	public static boolean checkColorful2(int no){
		String num = String.valueOf(no);
		if(num.contains("0") || num.contains("1"))
			return false;

		char[] arr = num.toCharArray();

		//check for duplicates
		Set<Byte> setB = new HashSet<Byte>();
		List<Integer> alist = new ArrayList<Integer>();
		for(char c: arr)
			if(!setB.add((byte)c))
				return false;
			else
				alist.add(Integer.parseInt(String.valueOf(c)));
		setB= null;int val;
		Set<Integer> setI = new HashSet<Integer>();
		for(int f=0;f<alist.size();f++){
			for(int l=f;l<alist.size();l++){
				val =1;
				for(int j=f;j<=l;j++){
					val+= alist.get(j);
					//System.out.println("value of j"+j);
				}
				System.out.println(" value is :"+val);
				if(!setI.add(val)) return false;
			}
		}
		return true;
	}


	//Given sorted array and a number find the max index of the number in the array

	public static int searchBin(int []A,int start,int last,int no,int found){
		if(start > last ){
			System.out.println("reached here");
			return found;
		}
		else{
			int mid = (start+last)/2;
			if(A[mid] == no){
				if(mid > found)
					found = mid;
				return searchBin(A,mid+1,last,no,found);
			}
			else if( A[mid] > no){
				return searchBin(A,start,mid-1,no,found);
			}
			else{
				return searchBin(A,mid+1,last,no,found);
			}
		}
	}


	//colorful number

	public static boolean checkFColorful(int no){
		String num = String.valueOf(no);
		if(num.contains("0") || num.contains("1"))
			return false;

		char[] arr = num.toCharArray();

		//check for duplicates
		Set<Byte> setB = new HashSet<Byte>();
		List<Integer> alist = new ArrayList<Integer>();
		for(char c: arr)
			if(!setB.add((byte)c))
				return false;
			else
				alist.add(Integer.parseInt(String.valueOf(c)));
		setB= null;int val;
		Set<Integer> setI = new HashSet<Integer>();
		for(int f=0;f<alist.size();f++){
			for(int l=f;l<alist.size();l++){
				val =1;
				for(int j=f;j<=l;j++){
					val*= alist.get(j);
					//System.out.println("value of j"+j);
				}
				//				System.out.println(" value is :"+val);
				if(!setI.add(val)) return false;
			}
		}
		return true;
	}

	///*******************************code to find if a number is magic or not**************************************************

	//magic number
	public static boolean isMagicNumber(int number){
		List<Integer> list = copyIntToArray(number);
		if(list.contains(0))
			return false;
		int value ;
		do {
			value =0;
			for(Integer i: list){
				value += i;
			}
			System.out.println("value : "+value);
			list =  copyIntToArray(value);
		} while(value > 9);
		if(value ==1) return true;
		else return false;
	}

	private static List<Integer> copyIntToArray(int number){
		char [] arr = String.valueOf(number).toCharArray();
		List<Integer> list = new ArrayList<Integer>();

		for(int i=0;i<arr.length;i++){
			list.add(Integer.parseInt(String.valueOf(arr[i])));
		}
		return list;
	}


	//****************************************end magic color******************************************************************

	//**************************************better code magic number **********************************************************
	//magic number short

	/**
	 * Magic number is a number whose digits add up to 1 eg 199 -> 1+9+9 => 19 -> 1+9 => 10 ->1+0 => 1 hence a magic number
     */

	public static void isMagic(int no){
		int s=0;
		while(no != 0){
			s+= (no%10);
			no = no/10;
		}
		if(s > 9)
			isMagic(s);
		if(s ==1)
			System.out.println("no is Magic::");
		if(s <=9 && s!=1 )
			System.out.println("No is not magic::");

	}

	public static int magic (int no){
		if(no < 9)
			return no;
		else
			return no%10 + magic(no/10);
	}



	public static boolean checkMagic (int  no){
		int j;
		while((j =magic(no))/10 !=0){
			System.out.println(" j :"+j);
			no =j;
		}
		if(j ==1)
			return true;
		else
			return false;
	}


	/**
	 *
	 * Example:
	 I/P    [1, 2, 4, ,6, 3, 7, 8]
	 O/P    5

	 * @param arr
	 * @return
	 *
	 * use formula total = n*(n-1)/2
	 *
	 */
	public static int getMissinNumber(int []arr){
		//for simplicity considering number starts with 1
		int total = ((arr.length+1)*(arr.length+2))/2;
		int sum=0;

		for(int i=0;i<arr.length;i++){
			sum += arr[i];
		}
		return total-sum;
	}


	//*************************************************end magic number *******************************************************



	//****************************Deci to Oct sample code *************************************


	public static  void deciToOct(int no,List<Integer> list){
		if (no > 8)
			deciToOct(no/8,list);
		list.add(no%8);
	}

	public static void printOct(int no){
		List<Integer> list = new ArrayList<Integer>();
		deciToOct(no,list);
		System.out.println(list.toString());
	}


	//******************************************end deci to oct******************************


	// is stepping number 

/*
Given two integers ‘n’ and ‘m’, find all the stepping numbers in range [n, m].
A number is called stepping number if all adjacent digits have an absolute difference of 1.
321 is a Stepping Number while 421 is not.
 */
	public static void steppingNos(int s,int e){
		if(e < s)
			System.out.println("error");
		else{
			while(s <= e ){
				if(isStepping(s))
					System.out.print(s+" ");
				s++;
			}
		}
	}

	public static boolean isStepping(int no){
		while ( no > 10){
			if(Math.abs((no%10) - ((no/10)%10)) == 1)
				no = no/10;
			else
				return false;
		}
		return true;
	}

	public static void getIndexProd(int number){
		int no = number;
		int newNo=0;
		long prod = 1l;
		while(no > 0){
			prod *= no%10;
			no = no/10;
		}
		//form the number
		no = number;
		for(int i=0;no >0 ; no=no/10,i++){
			int digit = no %10;
			newNo += (prod/digit)*Math.pow(10,i);
			if((prod/digit) >9)
				i++;
		}
		System.out.println("new no is :"+newNo);
	}


	//print matrix in spiral form
	public static void printSpiralMatrix(int [][]A){
		int n = A.length;
		int m = A[0].length;
		int x=0,y=0;
		while(n > 0 && m >0){
			//no more elements to circle
			if(m ==1){
				for(int i=0;i<m;i++)
					System.out.println(A[x][y++]);
				break;
			}
			if(n ==1){
				for(int i=0;i<m;i++)
					System.out.println(A[x++][y]);
				break;
			}

			// move in circle 

			//left to right
			for(int i=0;i<m-1;i++){
				System.out.println(A[x][y++]);
			}
			//right to down
			for(int i=0;i<n-1;i++)
				System.out.println(A[x++][y]);

			//bottom right to left
			for(int i=0;i<m-1;i++)
				System.out.println(A[x][y--]);

			//bottom to top
			for(int i=0;i<m-1;i++)
				System.out.println(A[x--][y]);

			m = m-2;
			n = n-2;
			x++;
			y++;
		}

	}
	// end of spiral matrix print 


	// find the no of rotations of a sorted array in logn time

	public static int rotationSortedArray(int []A){
		int low =0;
		int high = A.length-1;
		while(A[low] > A[high]){
			int mid =  (high+low)>>>1;//same as low+high / 2
			if(A[mid] > A[high]){
				low = mid+1;
			}
			else
				high = mid;
		}
		return low;
	}




	// find the longest sub-array with equal no. of  zeroes and ones 



	public static void findZeroOne(int []A){
		int count1=0,count0=0;
		int start =0,end =0;int maxlen=0;
		for(int i=0;i<A.length;i++){
			if(A[i] == 0)
				count0++;
			else
				count1++;
			if(count0 == count1){
				start = i- Math.min(count1,count0)*2;
				end=i;
				maxlen = Math.min(count1,count0)*2;
			}
			else{
				//System.out.println("math min is :"+ Math.min(count1, count0) + " start :"+start);
				start = i - (Math.min(count1,count0)*2-1);
				end =i;
				//System.out.println("start after change is  :"+ start);
				maxlen = Math.min(count1,count0)*2;
			}
		}
		System.out.println("maxlen is :"+maxlen+" start :"+start+" end :"+end);
	}



	//find the sq root of a number

	public static int sqrt(int x){
		int left =1,right=x;
		while(true){
			int mid = (left+right)/2;
			if(mid > x/mid){
				right =mid-1;
			}else if(mid+1 > x/(mid+1)){
				return mid;
			}else{
				left= mid+1;
			}
		}

	}








}





























