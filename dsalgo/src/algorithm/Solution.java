package algorithm;

import java.io.*;
import java.util.*;

/**
 // Code for  a magic square of order 3

 // |8 |1 |6 | = 15 = the Same number
 // |3 |5 |7 | = 15
 // |4 |9 |2 | = 15
 // 15  15 15
 // 1. Go diagonal, go downward
 // 2. If an item is already there go downward
 */

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */

class Solution {
    public static void main(String[] args) {
        int[][] arr = new int[3][3];
        int x = 0, y = 0;
        int count = 1;
        arr[0][0] = count;
        while (count <= 9) {
            if (arr[x][y] == 0) {
                // move diagonal
                x += 1;
                y += 1;
                x = x == 3 ? 0 : x;
                y = y == 3 ? 0 : y;
                //move down
                y += 1;
                y = y == 3 ? 0 : y;
            }
            if (arr[x][y] == 0) {
                //go down here
                y += 1;
                y = y == 3 ? 0 : y;
            }
            count++;
            arr[x][y] = count;
        }
    }
}


