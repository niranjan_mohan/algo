package algorithm;

import java.util.ArrayList;
import java.util.HashMap;

class Phon {

	static ArrayList<String> printAllCombination(HashMap<Character, String> map, String input) {
		ArrayList<String> result = new ArrayList<String>();
		if (input.length() == 0) {
			return result;
		}
		char first = input.charAt(0);
		ArrayList<String> list = printAllCombination(map, input.substring(1));
		for (int i = 0; i < map.get(first).length(); i++) {
			if (list.size() == 0) {
				String t=""+map.get(first).charAt(i);
				result.add(t);
			} else {
				for (String s : list) {
					s = map.get(first).charAt(i) + s;
					result.add(s);
				}
			}
		}

		return result;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		HashMap<Character, String> list = new HashMap<Character, String>();
		list.put('1', "NULL");
		list.put('2', "ABC");
		list.put('3', "DEF");
		list.put('4', "GHI");
		list.put('5', "JKL");
		list.put('6', "MON");
		list.put('7', "PORS");
		list.put('8', "TUV");
		list.put('9', "WXYZ");
		System.out.println(printAllCombination(list, "23457"));
	}

}
