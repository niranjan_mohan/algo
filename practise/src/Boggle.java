import java.util.List;
import java.util.Set;

public class Boggle {


    Set<String> dict;
    public static void main(String []args){

    }



    public void getWords(char[][] board,int x,int y,boolean [][] isVisited,StringBuilder curWord,List<String> result){
        int len = board.length;
        if(x <0 || y< 0 || x>= len || y>= len || isVisited[x][y]){
            return ;
        }
        if(dict.contains(curWord.toString())){
            result.add(curWord.toString());
        }

        curWord.append(board[x][y]);
        isVisited[x][y] = true;

        getWords(board,x+1,y,isVisited,curWord,result);
        getWords(board,x-1,y,isVisited,curWord,result);
        getWords(board,x,y+1,isVisited,curWord,result);
        getWords(board,x,y-1,isVisited,curWord,result);
        getWords(board,x-1,y-1,isVisited,curWord,result);
        getWords(board,x+1,y+1,isVisited,curWord,result);

        curWord = new StringBuilder();
        isVisited[x][y] = false;


    }


}
