package Questions.src;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by nirmohan on 9/13/17.
 */
public class Test5 {


    public static void main(String []args){



                    /*
            III. Given three letters ABC, where AB->C, AC->B, BC->A (sequence doesn’t matter). Get the length of the path to convert from a given string to a single character.


            For example, “ABACB” goes to “ACCB” (based on AB ->C, convert s[1] and s[2] to C)
            “ACCB” goes to “BCB” (based on AC->B)
            “BCB” goes to “AB”
            “AB” goes to “C”
            So it takes 4 steps to change the given string into a single character.
            If a given string cannot be resized to 1 character, such as “AAA” or "ABACABB", return -1.

            */
//
//        String a = "abc";
//        System.out.println("substring "+a.substring(2,3));
//        Map<String,String> dictMap = new HashMap<String,String>();
//        dictMap.put("AB","C");
//        dictMap.put("AC","B");
//        dictMap.put("BC","A");
//        System.out.println("Count steps ::"+countSteps(dictMap,"ABACB"));
//
//
//        System.out.println("**************Tester **************");
//        int x= 24;
//        System.out.println("x   "+(x >>1)+"  ::"+(x>>2)+"   ::"+(x<<2)+"     :"+(x<<1));
//
//
//
//
//
//
//
//        String stest = "niranjan";
//        System.out.println("substring of :"+stest.substring(2));


    }





    public static int countSteps(Map<String,String> dictRules, String s){
        int []result = new int[1];
        result[0] = -1;
      reduceString(s,dictRules,null,0,0,result) ;
        return result[0];
    }


    public static boolean reduceString(String s, Map<String,String> dictRules ,String v, int tryIndex, int count,int []result){

        if(v!=null){
            StringBuilder stb = new StringBuilder(s);
            stb.insert(tryIndex,v);
            s= stb.toString();
        }

        if(s.length() ==1){
            result[0] =count;
            return true;
        }
        if(result[0] == -1) {
            for (int i = 0; i < s.length() - 1; i++) {
                String find = s.substring(i, i + 2);
                if (dictRules.containsKey(find)) {
                    String replaceString = s.replace(find, "");
                    while (!reduceString(replaceString, dictRules, dictRules.get(find), tryIndex, count + 1, result) && tryIndex < replaceString.length()) {
                        tryIndex++;
                    }
                }
            }
        }
        return false;
    }


//power of number w/o using *


    /*

        Sum of Odd numbers
    1*1 = 1 // 1
    2*2 = 4 // 1+3
    3*3 = 9 // 1+3+5
    4*4 =16// 1+3+5+7
     */



    public static int power(int no){
        int sum=0;
        for(int i=0,j=1;i<no;i++,j+=2){
            sum+=j;
        }
        return sum;
    }




    //log (n) solution


    public static int getProd(int n,int m){
        if(n==1){
            return m;
        }
        if(n%2 ==0){
            return getProd(n>>1 , m<<1);
        }else{
            return  getProd(n>>1,m<<1) + m;
        }

    }




    //sum 2 numbers w/0 using +

    public static int findSum(int n1,int n2){
        while(n1 >0){
            n1--;n2++;
        }
        return n2;
    }


}


/*
You have a 100 coins laying flat on a table, each with a head side and a tail side.
10 of them are heads up, 90 are tails up. You can't feel, see or in any other way find out which side is up.
Split the coins into two piles such that there are the same number of heads in each pile.
 */
