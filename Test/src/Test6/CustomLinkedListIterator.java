package Test6;

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Created by nirmohan on 9/14/17.
 */
public class CustomLinkedListIterator<T extends Object > implements Iterator<T> {


    LinkedList<LinkedList<T>> items ;
    Iterator <LinkedList<T>> parentIt;
    Iterator <T> childIt;


    public CustomLinkedListIterator (LinkedList<LinkedList<T>> linkedLists){
        this.items = linkedLists;
    }


    public boolean hasNext(){
        if(parentIt ==null){
            return false;
        }
        parentIt = items.iterator();
        while(parentIt.hasNext()){
            LinkedList child = parentIt.next();
            childIt = child.iterator();
            if(childIt !=null && childIt.hasNext()){
                return true;
            }
        }
        return false;
    }


    public T next(){
        if(childIt !=null && childIt.hasNext()){
            return childIt.next();
        }else{
            while(parentIt.hasNext()){
                LinkedList<T> child = parentIt.next();
                if(child !=null) {
                    childIt = child.iterator();
                    if (childIt.hasNext()) {
                        return childIt.next();
                    }
                }
            }
        }
        return null;
    }
}
