package Questions.src;

import java.util.*;

/**
 * Created by nirmohan on 9/30/17.
 */
public class Test9 {

    public static void main(String []args) {

        int [] input = {1,1,1,1,1,1,1,2};
        System.out.println("no of rabits " +numRabbits(input));

        int [] primes = {1, 2, 3, 5};
        kthSmallestPrimeFraction(primes,3);



    }


    public static  int numRabbits(int[] a) {
        Arrays.sort(a);
        int total = 0, r = 0;
        for(int i = 0; i < a.length; i++) {
            if (r-- == 0 || a[i] != a[i-1]) {
                r = a[i];
                total += a[i]+1;
            }
        }
        return total;
    }



    public static int[] kthSmallestPrimeFraction(int[] a, int k) {
        int n = a.length;
        // 0: numerator idx, 1: denominator idx
        PriorityQueue<int[]> pq = new PriorityQueue<>(new Comparator<int[]>() {
            @Override
            public int compare(int[] o1, int[] o2) {
                int s1 = a[o1[0]] * a[o2[1]];
                int s2 = a[o2[0]] * a[o1[1]];
                return s1 - s2;
            }
        });
        for (int i = 0; i < n-1; i++) {
            pq.add(new int[]{i, n-1});
        }
        for (int i = 0; i < k-1; i++) {
            int[] pop = pq.remove();
            int ni = pop[0];
            int di = pop[1];
            if (pop[1] - 1 > pop[0]) {
                pop[1]--;
                pq.add(pop);
            }
        }

        int[] peek = pq.peek();
        return new int[]{a[peek[0]], a[peek[1]]};
    }


        // kth smallest element in a sorted array (runs in 1ms)
        public int kthSmallest(int[][] matrix, int k) {
            int lo = matrix[0][0];
            int hi = matrix[matrix.length - 1][matrix[0].length - 1];
            while(lo < hi){
                int mid = lo + (hi - lo) / 2;
                int count = 0;
                int j = matrix[0].length - 1;
                for(int i = 0; i < matrix.length; i++){
                    while(j >= 0 && matrix[i][j] > mid){
                        j--;
                    }
                    count += j + 1;
                }
                if(count < k){
                    lo = mid + 1;
                }else{
                    hi = mid;
                }
            }
            return lo;
        }











}
