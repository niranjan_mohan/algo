package thread.thread;

public class DoubleCheckIdiom {
	private  volatile DoubleCheckIdiom instance;
	private DoubleCheckIdiom(){
		
	}
	public DoubleCheckIdiom getInstance(){
		DoubleCheckIdiom tempInstance = instance;
		if(tempInstance == null){
			synchronized(this){
				tempInstance = instance;
				if(tempInstance == null)
					instance = new DoubleCheckIdiom();
			}
		}
		return instance;	
	}
}
