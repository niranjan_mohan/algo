package thread.thread;

/**
 * 
 * use Jconsole to analyze the deadlock 
 * @author niranjan
 *
 */




class DeadLock{
	String str1 = "check debit balance";
	String str2 = "check credit balance";
	public void  creditCheck()  {
		while(true){
			synchronized(str2){
				System.out.println("got lock of credit");
//				Thread.sleep(10);
				synchronized(str1){
					System.out.println("got the second lock");
				}
			}
			System.out.println("released both the locks credit functions");
		}
	}

	public void  debitCheck() {
		while(true){
			synchronized(str1){
				System.out.println("got lock of debit");
//				Thread.sleep(10);
				synchronized(str2){
					System.out.println("got the second lock");
				}
			}
			System.out.println("released both the locks debit functions");
		}
	}
	Thread t1 = new Thread(){
		public void run(){
			creditCheck();
		}
	};
	Thread t2 = new Thread(){
		public void run(){
			debitCheck();
		}
	};


	public static void main(String [] args){
		DeadLock dlock = new DeadLock();
		dlock.t1.start();
		dlock.t2.start();
		System.out.println("started both the threads");
	}
}

