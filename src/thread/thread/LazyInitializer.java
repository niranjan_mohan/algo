package thread.thread;

public class LazyInitializer {
	private LazyInitializer(){};
	private  static class LazyHolder {
		public static final LazyInitializer INSTANCE = new LazyInitializer();
	}
	public static LazyInitializer getInstance(){
		return LazyHolder.INSTANCE;
	}

}
