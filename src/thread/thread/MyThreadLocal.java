package thread.thread;

import java.util.Map;
import java.util.StringJoiner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by nirmohan on 6/10/17.
 */
public class MyThreadLocal {

//todo : implement readwrite lock in some usecase
    private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
    private final Lock r = rwl.readLock();
    private final Lock w = rwl.writeLock();

    private final static ThreadLocal<Map<String,String>> mymap = new ThreadLocal<Map<String,String>>(){
        @Override
        protected Map initialValue() {
            return new ConcurrentHashMap<String,String>();
        }
    };


    public static void main(String []args){
        StringBuilder stb = new StringBuilder();
        System.out.println("blank obj "+stb.toString());
    }
}
