package oops.trafficsignal;

public enum LightState {
	RED(1),
	ORANGE(2),
	GREEN(3);
	int lno;
	LightState(int lno){
		this.lno = lno;
	}
	LightState(){
		
	}

}
