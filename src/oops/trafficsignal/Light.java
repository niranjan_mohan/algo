package oops.trafficsignal;

public class Light {
	String direction;
	LightState lstate;
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public LightState getLstate() {
		return lstate;
	}
	public void setLstate(LightState lstate) {
		this.lstate = lstate;
	}

}
