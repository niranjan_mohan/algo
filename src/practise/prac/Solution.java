package practise.prac;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by niranjan on 8/22/16.
 */
public class Solution {
    public boolean isIsomorphic(String s, String t) {
        if(s.length() != t.length())
            return false;
        Map<Character,Character> cmap = new HashMap<Character,Character>();
        for(int i=0;i<s.length();i++){
            if(cmap.containsKey(s.charAt(i))){
                if(! (cmap.get(s.charAt(i)) == t.charAt(i))){
                    return false;
                }
            }else{
                cmap.put(s.charAt(i),t.charAt(i));
            }
        }
        return true;
    }
}