package practise.prac;
/**
 * Created by niranjan on 7/21/16.
 */
public class LinkedList {
    Node headNode;
    Node h1;
    Node h2;
    Node curNode;


    public void add(int no){
        Node n = new Node(no);
        if(headNode ==null){
            headNode =n;
        }else{
            curNode = headNode;
//            System.out.println("test " +curNode.value+ "headNode"+headNode.value);
            while(curNode.next != null)
               curNode = curNode.next;
            curNode.next =n;
            curNode = curNode.next;
        }
    }





    public Node addNumbers(Node h1,Node h2,int carry){
        if(h1 ==null && h2 ==null && carry == 0){
            return null;
        }
        Node n = new Node();
        int val =0;
        if(h1 != null){
            val+=h1.value;
        }
        if(h2 != null){
            val+=h2.value;
        }
        val+=carry;
        n.value = val%10;
        Node more = addNumbers(h1 !=null ? h1.next:null,
                h2!= null ? h2.next:null,
                val >9 ? 1:0);
        n.next = more;
        return n;
    }





    public  void kthLast(){
//        if(ll.size() <=k)
        Index index = new Index();
        index.indexVal = 0;
        kthLast(headNode,index,4 );

    }
    class Index{
        int indexVal;
    }



    private void displayAll(Node headNode){
        curNode = headNode;
        System.out.println();
        while(curNode != null){
            System.out.print(" -> "+curNode.value);
            curNode = curNode.next;
        }
    }

    public void  addNumbers(){
        displayAll(addNumbers(h1,h2,0));
    }



    public Node addNode(Node l1,Node l2,int carry){
        if(l1 ==null && l2 ==null && carry ==0)
            return l1;

        int value =carry;
        if(l1 != null )
            value+=l1.value;
        if(l2!=null)
            value+=l2.value;
        Node node = new Node(value%10);
        Node more = addNode(l1.next,l2.next,value >9 ?1:0);
        node.next =more;
        return node;
    }

    public void kthLast(Node node, Index index, int k){
        if(node != null)
             kthLast(node.next,index,k);
//        System.out.println("value k"+k+"index"+index);
        index.indexVal+=1;
        if(index.indexVal == k)
            System.out.println(node.value);
    }

    public void displayAll(){
        curNode = headNode;
        System.out.println();
        while(curNode != null){
            System.out.print(" -> "+curNode.value);
            curNode = curNode.next;
        }
    }

    public void displayAll(int no){
        Node localHead = no ==1 ?h1:h2;
        System.out.println();
        curNode = localHead;
        while(curNode != null){
            System.out.print(" -> "+curNode.value);
            curNode = curNode.next;
        }
    }

    public boolean deleteNode(int value){
        curNode = headNode;
        if(curNode.value == value){
            headNode = headNode.next;
            return true;
        }
        Node prevNode ;
        while(curNode != null){
            prevNode = curNode;
            curNode = curNode.next;
            if (curNode.value == value){
                prevNode.next = curNode.next;
                return true;
            }
        }
        return false;
    }

    // Node inner class defined here
    class Node{
        Node next;
        int value;
        Node(int value){
            this.value = value;
        }
        Node (){

        }
    }





    public void addh1(int no){
        Node n = new Node(no);
        if(h1 ==null){
            h1 =n;
        }else{
            curNode = h1;
//            System.out.println("test " +curNode.value+ "headNode"+h1.value);
            while(curNode.next != null)
                curNode = curNode.next;
            curNode.next =n;
            curNode = curNode.next;
        }
    }

    public void addh2(int no){
        Node n = new Node(no);
        if(h2 ==null){
            h2 =n;
        }else{
            curNode = h2;
//            System.out.println("test " +curNode.value+ "headNode"+headNode.value);
            while(curNode.next != null)
                curNode = curNode.next;
            curNode.next =n;
            curNode = curNode.next;
        }
    }



    public void addNumbersInOrder(){
        if(h1 ==null && h2==null)
        return;
        int len1 = sizeOf(h1);
        int len2 = sizeOf(h2);
        if(len1 > len2 )
            h2 = padNode(h2, len1-len2);
        if(len2>len1){
            h1 = padNode(h1,len2-len1);
        }
        Sum sum= new Sum();
        Node result =addNumberHelper( h1, h2, sum);
        if(sum.carry >0) result= addBeforeNode(result,1);
        displayAll(result);
    }

    public int sizeOf(Node n){
        int size=0;
        while(n!=null){
            size++;
            n=n.next;
        }
        return size;
    }



    class Sum{
        int sum;
        int carry;
    }



    public Node addNumberHelper(Node h1,Node h2,Sum sum){
        if(h1 == null && h2 ==null && sum.carry == 0){
            return null;
        }
        Node more = addNumberHelper(h1!=null ? h1.next:null, h2!=null ? h2.next:null, sum);
        Node result;
        int value = 0;
        if(h1 != null) value+=h1.value;
        if(h2!= null) value+=h2.value;
        if(sum != null) value+=sum.carry;
        sum.sum = value%10;
        sum.carry = value>9?1:0;
        if(more !=null)
            result = addBeforeNode(more,sum.sum);
        else
            result = new Node(sum.sum);
        return result;

    }


    public Node padNode(Node node,int incrSize){
        for(int i=0;i<incrSize;i++){
            node =addBeforeNode(node,0);
        }
        return node;
    }

    public Node addBeforeNode(Node node,int value){
        if (node ==null)
            return null;
        Node n = new Node(value);
        n.next = node;
        return n;
    }

}
