package practise.prac;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by niranjan on 5/13/17.
 */
public class TestInversin {



    public static void main(String [] args) {
        TestInversin testInversin = new TestInversin();
        Integer Arr[] = { 0, 2, 1, 3, 5};
        List list =Arrays.asList(Arr);
        ListObj listObj = testInversin.initialize(list);
        ListObj result = testInversin.findInversion(listObj);
        System.out.println("inversions ::"+result.inv);
        System.out.println("array sorted ::"+result.ls.toString());

    }

    public ListObj initialize(List ls) {
        ListObj obj = new ListObj();
        obj.ls = ls;
        return obj;
    }


    class ListObj{
        ListObj(){

        }
        ListObj(List ls){
            this.ls = ls;
        }
         List<Integer> ls;
         int inv;
        public int size(){
            return ls.size();
        }
    }

    //count inversions
    public  ListObj findInversion(ListObj listObj){
        if(listObj ==null || listObj.size() <=1){
            return listObj;
        }
        ListObj left = new ListObj();
        ListObj right = new ListObj();
        int mid = listObj.size()/2;
         left.ls = listObj.ls.subList(0,mid);
         right.ls = listObj.ls.subList(mid,listObj.size());
        left = findInversion(left);
        right = findInversion(right);
        ListObj result = mergeInv(left,right);
        return result;
    }


    public  ListObj mergeInv(ListObj left,ListObj right){
        ListObj result = new ListObj(new ArrayList<Integer>());
        Iterator<Integer> itl = left.ls.iterator();
        Iterator<Integer> itr = right.ls.iterator();
        int l = itl.next();
        int r = itr.next();
        while(true){
            if(l < r){
                result.ls.add(l);
                if(itl.hasNext())
                    l = itl.next();
                else{
                    result.ls.add(r);
                    while(itr.hasNext()){
                        result.ls.add(itr.next());
                    }
                    break;
                }
            }else{//r< l
                result.ls.add(r);
                result.inv+=1;
                if(itr.hasNext()){
                    r = itr.next();
                }else{
                    result.ls.add(l);
                    while(itl.hasNext()){
                        result.ls.add(itl.next());
                        result.inv+=1;
                    }
                    break;
                }
            }
        }
        return result;
    }

}




