package practise.prac;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collector;

/**
 * Created by nirmohan on 6/18/17.
 */
public class TestRunner {


    public static  void main(String [] args){
        TestRunner testRunner = new TestRunner();
        List<String> result = testRunner.getPermutes("Hello");
        System.out.println("The permutes are ::"+result.toString());
    }

    public List<String> getPermutes(String str){
        if(str ==null)
            return null;
        if(str.length() <=1)
            return null;
        List<String> result = new ArrayList<String>();
        Map<Character,Integer> freqMap= buildFreqMap(str);
        permute(freqMap,str.length(),"",result);
        return result;
    }

    public Map<Character,Integer> buildFreqMap(String str){
        if (str == null)
            return null;
        Map<Character,Integer> freqMap = new HashMap<Character,Integer>();
        for(int i=0;i<str.length();i++){
            if(!freqMap.containsKey(str.charAt(i))){
                freqMap.put(str.charAt(i),1);
            }else{
                Character ch = str.charAt(i);
                freqMap.put(ch,freqMap.get(ch)+1);
            }
        }
        return freqMap;
    }

    public void permute(Map<Character,Integer> freqMap,int remaining,String prefix,List<String> result){
        if(remaining ==0){
            result.add(prefix);
            return;
        }
        for(Character ch : freqMap.keySet()){
            int count = freqMap.get(ch);
            if(count >0){
                freqMap.put(ch,count-1);
                permute(freqMap,remaining-1,prefix+ch,result);
                freqMap.put(ch,count);
            }
        }
    }
}
