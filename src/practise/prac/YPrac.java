package practise.prac;

import java.util.List;

/**
 * Created by nirmohan on 6/10/17.
 */
public class YPrac {

    public static void main(String []s){
        String str = "reverseme";
        System.out.println(reverse(str));

    }
    public static String reverse(String str){
        if(str == null || str.length() <=1)
            return str;
        int end = str.length()-1;
        int start =0 ;
        char [] arr = str.toCharArray();
        while(start < (str.length()-1)/2){
            //swap start and end index
            char temp = arr[start];
            arr[start]= arr[end];
            arr[end]= temp;
            start++;end--;
        }
        return String.valueOf(arr);
    }

    //find the first non trailing zero
    public int findMaxCount(List<Integer> seq){
        int maxlen=0;
        int countsbefore=0,count1=0;
        for(int i=0;i<seq.size();i++){
            if(seq.get(i) !=0){
                count1++;
            }else{
                if(maxlen < count1+ countsbefore +1){
                    maxlen = count1 + countsbefore +1;
                }
                //test if non trailing zero
                if(i +1 < seq.size() && seq.get(i+1) !=0){
                    countsbefore = count1;
                }
                count1 = 0;
            }
        }
        return maxlen;
    }



}
