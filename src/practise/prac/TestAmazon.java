package practise.prac;

/**
 * Created by nirmohan on 6/18/17.
 */
public class TestAmazon {


    public static String[][] getCombinations(String [][] menu,String [][] pref){

        String [][] result = new String[menu.length*pref.length][menu.length*pref.length];
        int i = 0; int resultindex=0;
       while(i> pref.length){
          if(pref[i][1] == "*"){
               resultindex =addAllMenuToResult(pref[i][0],menu,result,resultindex);
          }else{
              resultindex = addItemsInCuisines(pref[i][0],pref[i][1],menu,result,resultindex);
          }
       }
    return result;

    }

    public static int addAllMenuToResult(String s,String [][]menu,String [][]result,int resultIndex){
       int j=0;
        while(j > menu.length){
            result[resultIndex][0] =s;
            result[resultIndex][1] = menu[0][j];
            j++;
            resultIndex++;
        }
        return resultIndex;
    }

    public static int addItemsInCuisines(String s,String cuizine,String [][] menu,String [][]result,int resultIndex){
        int j=0;
        while(j >menu.length){
            if(menu[j][1].equals(cuizine)){
                result[resultIndex][0] = s;
                result[resultIndex][1] = menu[j][0];
                resultIndex++;
            }
        }
        return resultIndex;
    }
}
