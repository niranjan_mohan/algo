package practise.prac;

import java.util.Queue;
import java.util.Stack;

/**
 * Created by niranjan on 5/15/17.
 */
class PracBTree{
    Node rootNode;





    public void addNode(int key,String value){
        Node n = new Node(key,value);
        if(rootNode == null){
            rootNode = n;
        }else{
            Node curNode = rootNode;
            Node parentNode = curNode;
            while(true){
                parentNode =curNode;
                if(key <curNode.key){
                    curNode = curNode.leftChild;
                    if(curNode == null){
                        parentNode.leftChild=n;
                        break;
                    }
                }else{
                    curNode = curNode.rightChild;
                    if(curNode == null){
                        parentNode.rightChild=n;
                        break;
                    }
                }
            }
        }
    }

    public void inOrder(){
        Node curNode=null;
        if(rootNode !=null)
            curNode=rootNode;
        inOrder(curNode);
    }

    public void inOrder(Node n){
        if(n != null){
            inOrder(n.leftChild);
            System.out.print(n.value+" ");
            inOrder(n.rightChild);
        }
    }

//    public void addNodeR()

    public void addNodeR(int key,String value){
        Node node = new Node(key,value);
        if(rootNode ==null)
            rootNode = node;
        else{
            addNodeR(rootNode,node);
        }
    }
    public Node addNodeR(Node curNode,Node n){
        if(curNode == null)
            return n;
        else if(n.key < curNode.key){
            curNode.leftChild = addNodeR(curNode.leftChild,n);
        }else{
            curNode.rightChild = addNodeR(curNode.rightChild,n);
        }
        return n;
    }

    public void preorderItr(){
        Stack<Node> s = new Stack<>();
        s.push(rootNode);
        while(!s.isEmpty()){
            Node n = s.pop();
            if(n.rightChild !=null)
                s.push(n.rightChild);
            if(n.leftChild!=null)
                s.push(n.leftChild);
            System.out.print("  :"+n.value);
        }
    }



    public void bfs(){
        Queue<Node> q = new java.util.LinkedList<Node>();
        q.offer(rootNode);
        while(!q.isEmpty()){
            Node n = q.poll();
            if(n.rightChild !=null){
                q.offer(n.rightChild);
            }
            if(n.leftChild!=null){
                q.offer(n.leftChild);
            }
            System.out.println(" :"+n.value);
        }
    }




    public static void main(String [] args){
        //Add
        PracBTree tree = new PracBTree();
        tree.addNode(6,"f");
        tree.addNode(2,"b");
        tree.addNode(1,"a");
        tree.addNode(4,"d");
        tree.addNode(5,"e");
        tree.addNode(2,"c");
        tree.addNode(7,"g");
        tree.addNode(9,"i");
        tree.addNode(8,"h");

//        tree.inOrder();

        tree.preorderItr();
        tree.bfs();





    }





    class Node {
        Node (int key,String value){
            this.key = key;
            this.value = value;
        }
        Node leftChild;
        Node rightChild;
        int key;
        String value;
    }
}

