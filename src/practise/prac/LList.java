package practise.prac;

/**
 * Created by niranjan on 5/15/17.
 */
class LList{
    Node rootNode;
    Node curNode;

    LList(int n){
        Node node = new Node(n);
        rootNode = node;
        curNode = rootNode;

    }

    public void addNode(int n){
        Node node = new Node(n);
        curNode.next =node;
        curNode = curNode.next;
    }
    public void printLL(){
        curNode = rootNode;
        while(curNode != null){
            System.out.print(curNode.value+",");
            curNode = curNode.next;
        }
        System.out.println();
    }


    public boolean deleteNode(int n){
        curNode = rootNode;
        Node tempNode;
        if(rootNode.value == n){
            rootNode = rootNode.next;
            curNode = rootNode;
            return true;
        }
        while(curNode != null){
            tempNode =curNode;
            curNode = curNode.next;
            if(curNode.value == n){
                tempNode.next = curNode.next;
                curNode = tempNode;
                return true;
            }
        }
        return false;
    }

    public void reversell(){
        curNode = rootNode;
        Node prevNode=null;Node nextNode;
        while(curNode !=null && curNode.next!=null){
            nextNode = curNode.next;
            curNode.next= prevNode;
            prevNode = curNode;
            curNode = nextNode;
        }
        curNode.next = prevNode;
        rootNode = curNode;
        printLL();
    }



    public static void main(String [] args){
        LList ll = new LList(1);
        ll.addNode(1);ll.addNode(2);ll.addNode(4);ll.addNode(3);ll.addNode(5);ll.addNode(6);
        ll.printLL();
        ll.deleteNode(4);
        ll.deleteNode(1);
        ll.printLL();
        ll.reversell();
        ll.reverseR();
    }

    public void reverseR(){
        reverseR(rootNode);
    }

    public void reverseR(Node rootNode){
        if(rootNode == null)
            return;
        reverseR(rootNode.next);
        System.out.print(rootNode.value+",");
    }

    class Node{
        Node(int value){
            this.value = value;
        }
        int value;
        Node next;
    }

}
